<?php
// --------------------------------------------------------------------------------
// Custom Code Example : Let ATiM generate aliquot barcode with specific format
// --------------------------------------------------------------------------------
// AliquotMaster Model Custom
// Path : \app\Plugin\InventoryManagement\Model\Custom
// File Name : AliquotMaster.php
// --------------------------------------------------------------------------------
class AliquotMasterCustom extends AliquotMaster
{

    var $useTable = 'aliquot_masters';

    var $name = "AliquotMaster";

    /**
     * Generate the next unique barcode to assign to a created aliquot then to validate and save.
     * Function will return a barcode with following format /ATiM\#[0-9]{11}/ like 'ATiM#00000000001'.
     *
     * @return string New barcode value to validate then save or empty string when no barcode has been generated.
     */
    public function generateNextAtimBarcode()
    {
        $nextGeneratedBarcodesToUse = null;
        if (empty($this->nextGeneratedBarcodesToUse)) {
            $motif = '^ATiM\#([0-9]{11})$';
            $maxBarcode = $this->find('first', array(
                'conditions' => array(
                    "AliquotMaster.barcode REGEXP '$motif'"
                ),
                'fields' => array(
                    'MAX(AliquotMaster.barcode) AS max_barcode'
                )
            ));
            if (! empty($maxBarcode[0]['max_barcode'])) {
                preg_match("/$motif/", $maxBarcode[0]['max_barcode'], $matches);
                $nextGeneratedBarcodesToUse = (int) ($matches[1]) + 1;
            } else {
                $nextGeneratedBarcodesToUse = 1;
            }
        } else {
            $nextGeneratedBarcodesToUse = array_shift($this->nextGeneratedBarcodesToUse);
        }
        $this->nextGeneratedBarcodesToUse = array(
            $nextGeneratedBarcodesToUse + 1
        );
        return 'ATiM#' . str_pad($nextGeneratedBarcodesToUse, 8, 0, STR_PAD_LEFT);
    }
}