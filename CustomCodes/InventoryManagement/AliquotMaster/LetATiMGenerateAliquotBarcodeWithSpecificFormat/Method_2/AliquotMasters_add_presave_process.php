<?php
// --------------------------------------------------------------------------------
// Custom Code Example : Let ATiM generate aliquot barcode with specific format
// --------------------------------------------------------------------------------
// AliquotMaster Controller Hook
// - Controller : AliquotMasters
// - Function : add()
// - Hook() $arg_1 : 'presave_process'
// Path : \app\Plugin\InventoryManagement\Controller\Hook
// File Name : AliquotMasters_add_presave_process.php
// --------------------------------------------------------------------------------
foreach ($this->request->data as &$setOfSampleAliquotsToComplete) {
    foreach ($setOfSampleAliquotsToComplete['children'] as &$newAliquotToComplete) {
        $newAliquotToComplete['AliquotMaster']['barcode'] = $this->AliquotMaster->generateNextAtimBarcode();
    }
}
// Force system to keep barcode values
$this->AliquotMaster->addWritableField(array(
    'barcode'
));