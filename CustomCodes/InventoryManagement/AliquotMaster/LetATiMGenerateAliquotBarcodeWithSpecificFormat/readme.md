###################################################################
#		Let ATiM generate aliquot barcode with specific format		#
###################################################################

# CUSTOM CODE DESCRIPTION: 

Make ATiM in charge of the creation of the barcode. In the example, the format of the barcode should be characters 'ATiM#' plus an incremental number displayed with 8 digits: [ATiM#00000001].

If you need an example of code to generate a 'random barcode', please review function generateNewAtimBarcode() of the AliquotMaster model.


# SQL STATEMENTS TO RUN:

createCtrnetClinicalBMIStructure.sql


# CUSTOM CODE / HOOK FILES:

METHOD 1

Run followin query "UPDATE versions SET permissions_regenerated = 0" and logout then login.

Core.php : 
-	Configure::write('hideAliquotBarcodeFieldOfAliquotCreationForms', true);
-	Configure::write('useATiMAliquotBarcodeGeneratorForEmptyBarcode', true);
-	Configure::write('ATiMAliquotBarcodeSystemLength', 13);

Model Custom : 
-	\app\Plugin\ClinicalAnnotation\Model\Custom\AliquotMaster.php


METHOD 2

Run followin query "UPDATE versions SET permissions_regenerated = 0" and logout then login.

Core.php : 
-	Configure::write('hideAliquotBarcodeFieldOfAliquotCreationForms', true);

Controller Hook :
-	\app\Plugin\InventoryManagement\Controller\Hook\AliquotMasters_add_presave_process.php
-	\app\Plugin\InventoryManagement\Controller\Hook\AliquotMasters_realiquot_presave_process.php

Model Custom : 
-	\app\Plugin\ClinicalAnnotation\Model\Custom\AliquotMaster.php


METHOD 3

Run followin query "UPDATE versions SET permissions_regenerated = 0" and logout then login.

Core.php : 
-	Configure::write('hideAliquotBarcodeFieldOfAliquotCreationForms', true);

Controller Hook :
-	\app\Plugin\InventoryManagement\Controller\Hook\AliquotMasters_add_postsave_process.php
-	\app\Plugin\InventoryManagement\Controller\Hook\AliquotMasters_realiquot_postsave_process.php

Model Custom : 
-	\app\Plugin\ClinicalAnnotation\Model\Custom\AliquotMaster.php



# PRINT(S) SCREEN(S): 

Printed screen(s) of before and after update are avalaible to show an overview.


# NOTES:

We strongly recommend to use the METHOD1 being the only one that will be followed by the process validating that barcode are unique.
