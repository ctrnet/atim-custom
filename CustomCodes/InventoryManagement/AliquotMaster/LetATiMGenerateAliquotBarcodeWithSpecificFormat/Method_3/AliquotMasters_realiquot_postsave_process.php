<?php
// --------------------------------------------------------------------------------
// Custom Code Example : Let ATiM generate aliquot barcode with specific format
// --------------------------------------------------------------------------------
// AliquotMaster Controller Hook
// - Controller : AliquotMasters
// - Function : add()
// - Hook() $arg_1 : 'presave_process'
// Path : \app\Plugin\InventoryManagement\Controller\Hook
// File Name : AliquotMasters_add_postsave_process.php
// --------------------------------------------------------------------------------
$this->AliquotMaster->completeBarcode();