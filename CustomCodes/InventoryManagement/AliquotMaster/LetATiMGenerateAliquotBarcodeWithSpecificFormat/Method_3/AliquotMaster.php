<?php
// --------------------------------------------------------------------------------
// Custom Code Example : Let ATiM generate aliquot barcode with specific format
// --------------------------------------------------------------------------------
// AliquotMaster Model Custom
// Path : \app\Plugin\InventoryManagement\Model\Custom
// File Name : AliquotMaster.php
// --------------------------------------------------------------------------------
class AliquotMasterCustom extends AliquotMaster
{

    var $useTable = 'aliquot_masters';

    var $name = "AliquotMaster";

    /**
     * Update all empty barcode with barcode with format /ATiM\#[0-9]{11}/ like 'ATiM#00000000001'.
     *
     * @return -
     */
    public function completeBarcode()
    {
        // Get all aliquots with no barcode
        $allAliquotsToProcess = $this->find('all', array(
            'conditions' => array(
                'AliquotMaster.barcode' => ''
            ),
            'recursive' => - 1
        ));
        $this->addWritableField(array(
            'barcode'
        ));
        foreach ($allAliquotsToProcess as $aliquotToProcess) {
            $aliquotMasterid = $aliquotToProcess['AliquotMaster']['id'];
            $aliquotBarcode = 'ATiM#' . str_pad($aliquotMasterid, 11, 0, STR_PAD_LEFT);
            if (true) {
                $aliquotMasterDataToUpdate = array(
                    'AliquotMaster' => array(
                        'barcode' => $aliquotBarcode
                    )
                );
                // The method below is the cleanest one because this one uses the 'save()' model function: good practice.
                // But this method will create 2 records in 'aliquot_masters_revs' table.
                $this->id = $aliquotMasterid;
                $this->data = null; // *** To guaranty no merge will be done with previous data ***
                if (! $this->save($aliquotMasterDataToUpdate, false)) {
                    AppController::getInstance()->redirect('/Pages/err_plugin_system_error?method=' . __METHOD__ . ',line=' . __LINE__, null, true);
                }
            } else {
                // We consider that the methodbelow is preferable with an existing risk
                // because this one uses written SQL queries directly executed into the database.
                $queryToUpdate = "UPDATE aliquot_masters SET barcode = '$aliquotBarcode' WHERE id = $aliquotMasterid;";
                $this->tryCatchQuery($queryToUpdate);
            }
        }
    }
}


