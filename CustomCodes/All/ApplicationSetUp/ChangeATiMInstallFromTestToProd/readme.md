######################################################
#	 Change the ATiM install from Test to Prod		 #
######################################################

# CUSTOM CODE DESCRIPTION: 

Change the ATiM install from Test to Prod.


# SQL STATEMENTS TO RUN:

-


# CUSTOM CODE / HOOK FILES:

In '\trunk_practices\app\Config\core.php', change core variable '$isTest' from 1 to 0.


# PRINT(S) SCREEN(S): 

Printed screen(s) of before and after custom code are avalaible to show an overview.


# NOTES:

-