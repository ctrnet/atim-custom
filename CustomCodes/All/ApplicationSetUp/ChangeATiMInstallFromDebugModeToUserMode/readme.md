##################################################################
#	 Change the ATiM install from debug mode to user mode		 #
##################################################################

# CUSTOM CODE DESCRIPTION: 

Change ATiM installation from debug mode to user mode to hidde debug kite, hide any developper warning and force system to create cache and use cache.


# SQL STATEMENTS TO RUN:

-


# CUSTOM CODE / HOOK FILES:

In '\trunk_practices\app\Config\core.php', change core variable '$debug' from 2 to 0.


# PRINT(S) SCREEN(S): 

Printed screen(s) of before and after custom code are avalaible to show an overview.


# NOTES:

-