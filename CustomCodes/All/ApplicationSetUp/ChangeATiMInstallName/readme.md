##########################################
#	 Change the ATiM install name		 #
##########################################

# CUSTOM CODE DESCRIPTION: 

Change the ATiM install name to the name of a bank or research axis or institution.


# SQL STATEMENTS TO RUN:

INSERT INTO i18n (id,en,fr) VALUES ('core_installname', 'CTRNet - Demo', 'CTRNet - Demo');
UPDATE versions SET permissions_regenerated = 0;


# CUSTOM CODE / HOOK FILES:

-


# PRINT(S) SCREEN(S): 

Printed screen(s) of before and after custom code are avalaible to show an overview.


# NOTES:

-
