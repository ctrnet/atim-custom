<?php

// --------------------------------------------------------------------------------
// Custom Code Example : Calculate body mass index
// --------------------------------------------------------------------------------
// EventMaster Model Custom
// Path : \app\Plugin\ClinicalAnnotation\Model\Custom
// File Name : EventMaster.php
// --------------------------------------------------------------------------------
class EventMasterCustom extends EventMaster
{

    var $useTable = 'event_masters';

    var $name = "EventMaster";

    /**
     * Returns true if all fields pass validation.
     *
     * Will validate the currently set data. Use Model::set() or Model::create() to set the active data.
     *
     * @param array $options
     *            An optional array of custom options to be made available in the beforeValidate callback
     * @return bool True if there are no errors
     */
    public function validates($options = array())
    {
        // Execute parent model validations
        $result = parent::validates($options);

        // Validate that the value 'Height' of the model data to save is not equal to 0 else generate an error (division by 0 error)
        if (array_key_exists('EventDetail', $this->data) && array_key_exists('height_m', $this->data['EventDetail']) && array_key_exists('weight_kg', $this->data['EventDetail'])) {
            if (is_numeric($this->data['EventDetail']['height_m']) && strlen($this->data['EventDetail']['height_m']) && ($this->data['EventDetail']['height_m'] * 1) == 0) {
                $this->validationErrors['height_m'][] = 'the height can not be equal to zero to calculate the bmi';
                $result = false;
            }
        }

        // Return results
        return $result;
    }

    /**
     * Called before each save operation, after validation.
     * Return a non-true result
     * to halt the save.
     *
     * @param array $options
     *            Options passed from Model::save().
     * @return bool True if the operation should continue, false if it should abort
     */
    public function beforeSave($options = array())
    {
        // When both 'Height' and 'Weight' fields are part of the model data to save, calculate the BMI (Body Mass Index) and set a new field to save in database table
        if (array_key_exists('EventDetail', $this->data) && array_key_exists('height_m', $this->data['EventDetail']) && array_key_exists('weight_kg', $this->data['EventDetail'])) {
            // Get data to save
            $heightM = $this->data['EventDetail']['height_m'];
            $weightKg = $this->data['EventDetail']['weight_kg'];
            // Calculate BMI
            $bmi = '';
            if (strlen($heightM) && strlen($weightKg)) {
                if ($heightM == 0) {
                    // No validation to do. Already done by model
                }
                $bmi = $weightKg / ($heightM * $heightM);
            }
            // Add the generated value to the data of the model to save
            $this->data['EventDetail']['bmi'] = $bmi;
            // Add value to the AppModel::$writableFields to force system to not remove generated value
            // from the data set to save (feature to prevent hacking comparing fields of the displayed form and submitted data)
            $this->addWritableField('bmi');
        }

        // Execute parent model beforeSave()
        $retVal = parent::beforeSave($options);

        // Return True if the operation should continue
        return $retVal;
    }
}    
