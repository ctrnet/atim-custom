<?php

// --------------------------------------------------------------------------------
// Custom Code Example : Calculate body mass index
// --------------------------------------------------------------------------------
// EventMaster Model Custom
// Path : \app\Plugin\ClinicalAnnotation\Model\Custom
// File Name : EventMaster.php
// --------------------------------------------------------------------------------
class EventMasterCustom extends EventMaster
{

    var $useTable = 'event_masters';

    var $name = "EventMaster";

    /**
     * Calculate the EventMaster BMI based on submitted data.
     * The function support both classic mode (add and edit mode) meaning a unique record has to be saved
     * and addGrid mode consisiting of the creation of many records.
     *
     * @param array $eventData
     *            Submitted data.
     * @param
     *            boolean or array $submittedDataValidatesOrerrorsTracking
     *            Boolean defining if the data has been validated or not. Classic mode only.
     *            List of errors to display (addGrid mode only). AddGrid mode only.
     * @return -
     */
    public function calculateParticipantBmis(&$eventData, &$submittedDataValidatesOrerrorsTracking)
    {
        if (array_key_exists('0', $eventData)) {
            // Creation of a new event record linked to a EventControl with 'use_addgrid' equal 1 to let users to create more than one event for the partcipant
            if (array_key_exists('EventDetail', $eventData['0']) && array_key_exists('height_m', $eventData['0']['EventDetail']) && array_key_exists('weight_kg', $eventData['0']['EventDetail'])) {
                $lineNbr = 0;
                foreach ($eventData as & $newUniqueEventRecord) {
                    $lineNbr ++;
                    $returnedErrors = $this->calculateBmi($newUniqueEventRecord);
                    if ($returnedErrors) {
                        list ($field_name, $errorMessage) = $returnedErrors;
                        $submittedDataValidatesOrerrorsTracking[$field_name][__($errorMessage)][] = $lineNbr;
                    }
                }
                // Add value to the AppModel::$writableFields to force system to not remove generated value
                // from the data set to save (feature to prevent hacking comparing fields of the displayed form and submitted data)
                $this->addWritableField('bmi');
            }
        } else {
            // Basic record
            if (array_key_exists('EventDetail', $eventData) && array_key_exists('height_m', $eventData['EventDetail']) && array_key_exists('weight_kg', $eventData['EventDetail'])) {
                $returnedErrors = $this->calculateBmi($eventData);
                if ($returnedErrors) {
                    list ($field_name, $errorMessage) = $returnedErrors;
                    $this->validationErrors[$field_name] = $errorMessage;
                    $submittedDataValidatesOrerrorsTracking = false;
                }
                // Add value to the AppModel::$writableFields to force system to not remove generated value
                // from the data set to save (feature to prevent hacking comparing fields of the displayed form and submitted data)
                $this->addWritableField('bmi');
            }
        }
    }

    /**
     * Calculate the EventMaster BMI for one record.
     *
     * @param array $uniqueEventData
     *            Unique Event data.
     * @return array Error data composed by the field in error and the message.
     */
    private function calculateBmi(&$uniqueEventData)
    {
        if (array_key_exists('EventDetail', $uniqueEventData) && array_key_exists('height_m', $uniqueEventData['EventDetail']) && array_key_exists('weight_kg', $uniqueEventData['EventDetail'])) {
            // Get data to save
            $heightM = str_replace(',', '.', $uniqueEventData['EventDetail']['height_m']);
            $weightKg = str_replace(',', '.', $uniqueEventData['EventDetail']['weight_kg']);
            $bmi = null;
            if (strlen($heightM) && strlen($weightKg)) {
                // Validate submitted data
                if (! is_numeric($heightM)) {
                    return array(
                        'height_m',
                        'error_must_be_float'
                    );
                }
                if (! is_numeric($weightKg)) {
                    return array(
                        'weight_kg',
                        'error_must_be_float'
                    );
                }
                // Validate that the value 'Height' of the model data to save is not equal to 0 else generate an error (division by 0 error)
                if (strlen($heightM) && ($heightM * 1) == 0) {
                    return array(
                        'height_m',
                        'the height can not be equal to zero to calculate the bmi'
                    );
                }
                // Calculate BMI
                $bmi = $weightKg / ($heightM * $heightM);
            }
            // Add the generated value to the data of the model to save
            $uniqueEventData['EventDetail']['bmi'] = $bmi;
        }
        return null;
    }
}   
