<?php
// --------------------------------------------------------------------------------
// Custom Code Example : Calculate body mass index
// --------------------------------------------------------------------------------
// EventMaster Controller Hook
//   - Controller : EventMasters
//   - Function : add()
//   - Hook() $arg_1 : 'presave_process'
// Path : \app\Plugin\ClinicalAnnotation\Controller\Hook
// File Name : EventMasters_add_presave_process.php
// --------------------------------------------------------------------------------

// Calculate participant bmi
if (! $eventControlData['EventControl']['use_addgrid']) {
    // Classic mode.
    $this->EventMaster->calculateParticipantBmis($this->request->data, $submittedDataValidates);
} else {
    // AddGrid mode: Creation of more than one record for the participant.
    $this->EventMaster->calculateParticipantBmis($this->request->data, $errorsTracking);
}