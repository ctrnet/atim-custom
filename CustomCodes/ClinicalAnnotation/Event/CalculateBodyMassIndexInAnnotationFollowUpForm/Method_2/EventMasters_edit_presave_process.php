<?php
// --------------------------------------------------------------------------------
// Custom Code Example : Calculate body mass index
// --------------------------------------------------------------------------------
// EventMaster Controller Hook
//   - Controller : EventMasters
//   - Function : edit()
//   - Hook() $arg_1 : 'presave_process'
// Path : \app\Plugin\ClinicalAnnotation\Controller\Hook
// File Name : EventMasters_edit_presave_process.php
// --------------------------------------------------------------------------------

// Calculate participant bmi
$this->EventMaster->calculateParticipantBmis($this->request->data, $submittedDataValidates);