INSERT INTO event_controls (id, disease_site, event_group, event_type, flag_active, detail_form_alias, detail_tablename, display_order, databrowser_label, flag_use_for_ccl, use_addgrid, use_detail_form_for_index) 
VALUES
(null, '', 'clinical', 'ctrnet workshop - bmi', 1, 'ctrnet_workshop_form_ed_clinical_bmis', 'ctrnet_workshop_ed_clinical_bmis', 0, 'lab|ctrnet workshop - bmi', 0, 1, 1);

DROP TABLE IF EXISTS ctrnet_workshop_ed_clinical_bmis;
CREATE TABLE IF NOT EXISTS ctrnet_workshop_ed_clinical_bmis (
    event_master_id int(11) NOT NULL,
    height_m decimal(10,2) DEFAULT NULL,
    weight_kg decimal(10,2) DEFAULT NULL,
    bmi decimal(10,2) DEFAULT NULL,
    KEY event_master_id (event_master_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
    
DROP TABLE IF EXISTS ctrnet_workshop_ed_clinical_bmis_revs;
CREATE TABLE IF NOT EXISTS ctrnet_workshop_ed_clinical_bmis_revs (
    event_master_id int(11) NOT NULL,
    height_m decimal(10,2) DEFAULT NULL,
    weight_kg decimal(10,2) DEFAULT NULL,
    bmi decimal(10,2) DEFAULT NULL,
    version_id int(11) NOT NULL AUTO_INCREMENT,
    version_created datetime NOT NULL,
    PRIMARY KEY (version_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        
ALTER TABLE ctrnet_workshop_ed_clinical_bmis
   ADD CONSTRAINT ctrnet_workshop_ed_clinical_bmis_ibfk_1 FOREIGN KEY (event_master_id) REFERENCES event_masters (id);

INSERT INTO structures(`alias`) VALUES ('ctrnet_workshop_form_ed_clinical_bmis');

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('ClinicalAnnotation', 'EventDetail', 'ctrnet_workshop_ed_clinical_bmis', 'height_m', 'float',  NULL , '0', 'size=4', '', '', 'height m', ''), 
('ClinicalAnnotation', 'EventDetail', 'ctrnet_workshop_ed_clinical_bmis', 'weight_kg', 'float',  NULL , '0', 'size=4', '', '', 'weight kg', ''), 
('ClinicalAnnotation', 'EventDetail', 'ctrnet_workshop_ed_clinical_bmis', 'bmi', 'float',  NULL , '0', 'size=4', '', '', 'bmi', '');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='ctrnet_workshop_form_ed_clinical_bmis'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='ctrnet_workshop_ed_clinical_bmis' AND `field`='height_m' AND `type`='float' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=4' AND `default`='' AND `language_help`='' AND `language_label`='height m' AND `language_tag`=''), 
'2', '12', 'body mass index', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='ctrnet_workshop_form_ed_clinical_bmis'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='ctrnet_workshop_ed_clinical_bmis' AND `field`='weight_kg' AND `type`='float' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=4' AND `default`='' AND `language_help`='' AND `language_label`='weight kg' AND `language_tag`=''), 
'2', '13', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='ctrnet_workshop_form_ed_clinical_bmis'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='ctrnet_workshop_ed_clinical_bmis' AND `field`='bmi' AND `type`='float' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=4' AND `default`='' AND `language_help`='' AND `language_label`='bmi' AND `language_tag`=''), 
'2', '14', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0');

INSERT INTO structure_validations(structure_field_id, rule, language_message) 
VALUES
((SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='ctrnet_workshop_ed_clinical_bmis' AND `field`='height_m'), 'notBlank', ''),
((SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='ctrnet_workshop_ed_clinical_bmis' AND `field`='weight_kg'), 'notBlank', '');

INSERT IGNORE INTO i18n (id,en,fr)
VALUES
('body mass index', 'BMI', 'BMI'),
('height m', 'Height (m)', 'Taille (m)'),
('weight kg', 'Weight (kg)', 'Poids (kg)'),
('the height can not be equal to zero to calculate the bmi', 'The height can not be equal to zero to calculate the bmi.', 'La hauteur ne peut pas être égale à zéro pour calculer le bmi.'),
('bmi', 'BMI', 'BMI'),
('ctrnet workshop - bmi', 'BMI', 'BMI');