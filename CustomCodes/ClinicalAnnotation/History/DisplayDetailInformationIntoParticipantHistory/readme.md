#################################################################
#	  Display detail information into Participant History		#
#################################################################

# CUSTOM CODE DESCRIPTION: 

Display detail information into Participant History for following records:
   - Test, result and unit for any Biology (CTRNet Demo) record.
   - isplay the icd10 code descritpion of a Primary Diagnosis record.
   - Display the icdO3 topography category of any Secondary Diagnosis record.


# SQL STATEMENTS TO RUN:

-


# CUSTOM CODE / HOOK FILES:

METHOD 1 (Biology, icd10, icd03 description)

Controller Hook : 
-	app\Plugin\ClinicalAnnotation\Controller\Hook\Participants_chronology_annotation.php
-	app\Plugin\ClinicalAnnotation\Controller\Hook\Participants_chronology_diagnosis.php
-	app\Plugin\ClinicalAnnotation\Controller\Hook\Participants_chronology_start.php

METHOD 2 (icd03 description)

Controller Hook : 
-	app\Plugin\ClinicalAnnotation\Controller\Hook\Participants_chronology_start.ph


# PRINT(S) SCREEN(S): 

Printed screen(s) are avalaible to show an overview.


# CUSTOM

-
