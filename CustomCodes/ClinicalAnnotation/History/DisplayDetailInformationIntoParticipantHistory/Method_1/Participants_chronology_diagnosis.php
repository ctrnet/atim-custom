<?php
// --------------------------------------------------------------------------------
// Custom Code Example : Display detail information into Participant History
// --------------------------------------------------------------------------------
// Participant Controller Hook
// Path : \app\Plugin\ClinicalAnnotation\Controller\Hook
// File Name : Participants_chronology_diagnosis.php
// --------------------------------------------------------------------------------
if (in_array('dx_primary', explode(',', $dx['DiagnosisControl']['detail_form_alias'])) && isset($dx['DiagnosisMaster']['icd10_code'])) {
    $chronolgyDataDiagnosis['chronology_details'] = $dx['DiagnosisMaster']['icd10_code'] . ' - ' . $this->CodingIcd10Who->getDescription($dx['DiagnosisMaster']['icd10_code']);
} elseif ($dx['DiagnosisControl']['detail_form_alias'] == 'dx_secondary' && isset($dx['DiagnosisMaster']['icd_0_3_topography_category'])) {
    $chronolgyDataDiagnosis['chronology_details'] = $this->StructureValueDomain->getValueToDisplay('icd_0_3_topography_categories', $dx['DiagnosisMaster']['icd_0_3_topography_category']);
}