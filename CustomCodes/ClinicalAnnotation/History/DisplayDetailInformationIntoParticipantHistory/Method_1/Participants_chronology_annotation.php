<?php
// --------------------------------------------------------------------------------
// Custom Code Example : Display detail information into Participant History
// --------------------------------------------------------------------------------
// Participant Controller Hook
// Path : \app\Plugin\ClinicalAnnotation\Controller\Hook
// File Name : Participants_chronology_annotation.php
// --------------------------------------------------------------------------------

switch ($annotation['EventControl']['detail_form_alias']) {
    case 'ctrnet_demo_ed_lab_biology':
        $biologyResult = $annotation['EventDetail']['biology_test_result'];
        // WARNING : Replace biology_test database value by the value to display from the drop down list
        $biologyTest = $this->StructureValueDomain->getValueToDisplay('ctrnet_demo_biology_tests', $annotation['EventDetail']['biology_test']);
        // WARNING : Replace biology_test_unit database value by the value to display from the drop down list
        $biologyResultUnit = $this->StructureValueDomain->getValueToDisplay('ctrnet_demo_biology_test_units', $annotation['EventDetail']['biology_test_unit']);
        // Build display
        $valueToDisplay = '';
        if (strlen($biologyResult)) {
            $valueToDisplay = "$biologyResult $biologyResultUnit";
        }
        if (strlen($biologyTest)) {
            // WARNING : Replace biology_test_unit database value by the value to display from the drop down list
            $valueToDisplay = $biologyTest . (strlen($valueToDisplay) ? ' : ' . $valueToDisplay : '');
        }
        $chronolgyDataAnnotation['chronology_details'] = $valueToDisplay;
        break;
    default:
}
