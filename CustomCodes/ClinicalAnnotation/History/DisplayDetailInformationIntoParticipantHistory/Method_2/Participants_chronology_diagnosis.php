<?php
// --------------------------------------------------------------------------------
// Custom Code Example : Display detail information into Participant History
// --------------------------------------------------------------------------------
// Participant Controller Hook
// Path : \app\Plugin\ClinicalAnnotation\Controller\Hook
// File Name : Participants_chronology_diagnosis.php
// --------------------------------------------------------------------------------
if ($dx['DiagnosisControl']['detail_form_alias'] == 'dx_secondary' && isset($dx['DiagnosisMaster']['icd_0_3_topography_category'])) {
    $chronolgyDataDiagnosis['chronology_details'] = $icdO3TopoCategories[$dx['DiagnosisMaster']['icd_0_3_topography_category']]
}