<?php
// --------------------------------------------------------------------------------
// Custom Code Example : Display detail information into Participant History
// --------------------------------------------------------------------------------
// Participant Controller Hook
// Path : \app\Plugin\ClinicalAnnotation\Controller\Hook
// File Name : Participants_chronology_start.php
// --------------------------------------------------------------------------------

// *** Get ICD O 3 Topography Categories ***

$icdO3TopoModel = AppModel::getInstance('CodingIcd', 'CodingIcdo3Topo', true);
$icdO3TopoCategories = $icdO3TopoModel::getTopoCategoriesCodes();