################################################
#	 No more than one reproductive history		 #
################################################

# CUSTOM CODE DESCRIPTION: 

Don't allow user to create more than one reproductive history per participant.


# SQL STATEMENTS TO RUN:

INSERT IGNORE INTO i18n (id,en,fr)
VALUES
('an infomration has already been recorded for this patient', 'An information . . . ', 'Une information . . . ');


# CUSTOM CODE / HOOK FILES:

METHOD 1

Model Custom : 
-	\app\Plugin\ClinicalAnnotation\Controller\Hook\ReproductiveHistories_add_format.php


# PRINT(S) SCREEN(S):
 
Printed screen(s) of before and after custom code are avalaible to show an overview.


# NOTES:

We recomment the use of the function find with the argument 'fisrt' instead of 'count' for performance reason 
$this->ReproductiveHistory->find('first', array( ...));