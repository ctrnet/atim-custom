<?php
// --------------------------------------------------------------------------------
// Custom Code Example : No more than one reproductive history per participant
// --------------------------------------------------------------------------------
// ReproductiveHistory Controller Hook
//   - Controller : ReproductiveHistories
//   - Function : add()
//   - Hook() $arg_1 : 'format'
// Path : \app\Plugin\ClinicalAnnotation\Controller\Hook
// File Name : ReproductiveHistories_add_format.php
// --------------------------------------------------------------------------------

// Check no ReproductiveHistory record already exists in the database for the studied participant
$alreadyCreated = $this->ReproductiveHistory->find('count', array(
    'conditions' => array(
        'ReproductiveHistory.participant_id' => $participantId
    ),
	'recursive' => - 1
));

if ($alreadyCreated) {
    // Reproductive history already exists. 
    // Set translated message to display it as warning message and redirect user to the ReproductiveHistory index form.
    $this->atimFlashWarning(__('an information has already been recorded for this participant'), '/ClinicalAnnotation/ReproductiveHistories/listall/' . $participantId);
    return;
}
