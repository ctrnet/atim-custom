<?php
// --------------------------------------------------------------------------------
// Custom Code Example : Generate Participant Identifier
// --------------------------------------------------------------------------------
// Participant Controller Hook
//   - Controller : Participants
//   - Function : add()
//   - Hook() $arg_1 : 'presave_process'
// Path : \app\Plugin\ClinicalAnnotation\Controller\Hook
// File Name : Participants_add_presave_process.php
// --------------------------------------------------------------------------------

if ($submittedDataValidates) {
    // Get last Participant.id recorded
    $lastParticipantId = $this->Participant->find('first', array(
        'fields' => array(
            'MAX(id) AS last_participant_id'
        ),
        'recursive' => - 1
    ));
    // Set generated data
    if (isset($lastParticipantId['0']['last_participant_id'])) {
        $this->request->data['Participant']['participant_identifier'] = ((int)$lastParticipantId['0']['last_participant_id'] + 1);
    } else {
        $this->request->data['Participant']['participant_identifier'] = 1;
    }
    // Add value to the AppModel::$writableFields to force system to not remove generated value
    // from the data set to save (feature to prevent hacking comparing fields of the displayed form and submitted data)
    $this->Participant->addWritableField('participant_identifier');
}
