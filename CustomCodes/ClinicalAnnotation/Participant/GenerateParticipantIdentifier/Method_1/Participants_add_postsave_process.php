<?php
// --------------------------------------------------------------------------------
// Custom Code Example : Generate Participant Identifier
// --------------------------------------------------------------------------------
// Participant Controller Hook
//   - Controller : Participants
//   - Function : add()
//   - Hook() $arg_1 : 'postsave_process'
// Path : \app\Plugin\ClinicalAnnotation\Controller\Hook
// File Name : Participants_add_postsave_process.php
// --------------------------------------------------------------------------------

// Build and run query to update directly the participant_identifier of the record just created in participants table.
// (participant_identifier will be equal to the participant id)
$queryToUpdate = "UPDATE participants SET participants.participant_identifier = participants.id WHERE participants.id = " . $this->Participant->id . ";";
// Launch query on 'participants'
$this->Participant->tryCatchQuery($queryToUpdate);
// Launch query on 'participants_revs'
$this->Participant->tryCatchQuery(str_replace("participants", "participants_revs", $queryToUpdate));
