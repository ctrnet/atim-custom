<?php
// --------------------------------------------------------------------------------
// Custom Code Example : Generate Participant Identifier
// --------------------------------------------------------------------------------
// Participant Controller Hook
//   - Controller : Participants
//   - Function : add()
//   - Hook() $arg_1 : 'postsave_process'
// Path : \app\Plugin\ClinicalAnnotation\Controller\Hook
// File Name : Participants_add_postsave_process.php
// --------------------------------------------------------------------------------

// Flush data of the Participant model already saved. 
$participantId = $this->Participant->id;
$this->Participant->data = array();

// Set id of the Participant model with the id of the participant that has been just created to force system to update
// this participant and not create a new one.
$this->Participant->id = $participantId;

// Build array of data to update
$dataToUpdate = array(
    'Participant' => array(
        'participant_identifier' => $participantId
    )
);

// Following two lines are not requested because no validation will be done on the submitted data.
// $this->Participant->writableFieldsMode = 'add';
// $this->Participant->addWritableField('participant_identifier');

// Update participant data and force system to not validate the data ('false')
$this->Participant->save($dataToUpdate, false);