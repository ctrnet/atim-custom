#############################################################################
#	 					Generate Participant Identifier						      #
#############################################################################

# CUSTOM CODE DESCRIPTION: 

Make ATiM system in charge of the creation of the Participant.participant_identifier. 
Value will be equal to the Participant.Id.


# SQL STATEMENTS TO RUN:

updateParticipantIdentifierInStructureFormat.sql


# CUSTOM CODE / HOOK FILES:

METHOD 1:

Controller Hook : 
-	app\Plugin\ClinicalAnnotation\Controller\Hook\Participants_add_postsave_process.php

METHOD 2: 

Controller Hook : 
-	app\Plugin\ClinicalAnnotation\Controller\Hook\Participants_add_postsave_process.php 

METHOD 3: 

Controller Hook : 
-	app\Plugin\ClinicalAnnotation\Controller\Hook\Participants_add_presave_process.php


# PRINT(S) SCREEN(S): 

Printed screen(s) of before and after update are avalaible to show an overview.


# NOTES:

The method 2 is the cleanest one because this one uses the 'save()' model function: good practice. 
But this method will create 2 records in 'participants_revs' table. 
So we consider that the method 1 is preferable with an existing risk because this one uses written SQL queries directly executed into the database.
The method 3 can be considered but this one can generate a validation error if the generated value has already been created into the database. 
