<?php
// --------------------------------------------------------------------------------
// Custom Code Example : Validate Last Chart Checked Date is less or equal
// to the current date
// --------------------------------------------------------------------------------
// Participant Controller Hook
//   - Controller : Participants
//   - Function : edit()
//   - Hook() $arg_1 : 'presave_process'
// Path : \app\Plugin\ClinicalAnnotation\Controller\Hook
// File Name : Participants_edit_presave_process.php
// --------------------------------------------------------------------------------

// Get Participant last_chart_checked_date date and compare it wwith today's date.
// If last_chart_checked_date > today then print error and return false.
if ($submittedDataValidates && array_key_exists('Participant', $this->request->data) && array_key_exists('last_chart_checked_date', $this->request->data['Participant'])) {
    if (strlen($this->request->data['Participant']['last_chart_checked_date']['year'])) {
        // Note that part of the function below can be added to a Participant model function to not duplicate code in add and edit hook codes.
        $lastChartChekedDat = $this->request->data['Participant']['last_chart_checked_date'];
        // Validate() function of the model has not been executed at this level of the code. Date format is an array.
        // Reformat date as string.
        $dateFunctionFormat = sprintf("Y-%s-%s", (strlen($lastChartChekedDat['month'])? 'm' : ''), (strlen($lastChartChekedDat['day'])? 'm' : ''));
        $formatedLastChartCheckedDate = sprintf("%s-%s-%s", $lastChartChekedDat['year'], $lastChartChekedDat['month'], $lastChartChekedDat['day']);
        if ($formatedLastChartCheckedDate > date($dateFunctionFormat)) {
            $this->Participant->validationErrors['last_chart_checked_date'][] = __('the last chart checked date must less or equal to the current date');
            $submittedDataValidates = false;
        }
    }
}