#################################################################################
#		Validate Last Chart Checked Date is less or equal to current date		#
#################################################################################

# CUSTOM CODE DESCRIPTION: 

Validate 'Last Chart Checked Date' submitted by the user is less or equal to the current date


# SQL STATEMENTS TO RUN:

-


# CUSTOM CODE / HOOK FILES:

METHOD 1:

Model Custom : 
-	\app\Plugin\ClinicalAnnotation\Model\Custom\Participant.php

METHOD 2:

Controller Hook : 
-	app\Plugin\ClinicalAnnotation\Controller\Hook\Participants_add_presave_process.php 
-	app\Plugin\ClinicalAnnotation\Controller\Hook\Participants_edit_presave_process.php


# PRINT(S) SCREEN(S): 

Printed screen(s) of before and after update are avalaible to show an overview.


# NOTES:

For any submitted data validation we strongly recommend to use the 'validate()' function of the models.

Also method 2 requires 2 hooks. 
We suggest to limit the number of hook files created to simplify code and future upgrades. 
And the Method 2 won’t guaranty that the value will be updated if another method is created to create/update participant record.

So we consider 1st method as better for long term support and data integrity.