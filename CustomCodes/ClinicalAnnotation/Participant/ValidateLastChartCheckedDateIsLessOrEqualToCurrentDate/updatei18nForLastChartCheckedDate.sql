INSERT IGNORE INTO i18n (id, en, fr) VALUE
('the last chart checked date must less or equal to the current date', 
'The Last Chart Checked date must less or equal to the current date.', 
'La ''Date de la dernière révision de données'' doit être plus petite ou égale à la date d''aujourd''hui.');