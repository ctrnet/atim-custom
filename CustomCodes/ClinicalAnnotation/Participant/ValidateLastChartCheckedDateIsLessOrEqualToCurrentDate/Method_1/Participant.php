<?php
// --------------------------------------------------------------------------------
// Custom Code Example : Validate Last Chart Checked Date is less or equal 
// to the current date
// --------------------------------------------------------------------------------
// Participant Model Custom
// Path : \app\Plugin\ClinicalAnnotation\Model\Custom
// File Name : Participant.php
// --------------------------------------------------------------------------------

class ParticipantCustom extends Participant
{

    var $useTable = 'participants';

    var $name = "Participant";

    /**
     * Custom validation: 
     * Get Participant last_chart_checked_date date and compare it wwith today's date.
     * If last_chart_checked_date > today then print error and return false.
     *
     * @param array $options
     *            An optional array of custom options to be made available in the beforeValidate callback
     * @return bool True if there are no errors
     */
    public function validates($options = array())
    {
        // ATiM framework validations call
        $result = parent::validates($options);

        // Custom validation
        if (array_key_exists('Participant', $this->data) && array_key_exists('last_chart_checked_date', $this->data['Participant'])) {
            if (strlen($this->data['Participant']['last_chart_checked_date'])) {
                $lastDateInput = $this->data['Participant']['last_chart_checked_date'];
                if ($lastDateInput > date('Y-m-d')) {
                    $this->validationErrors['last_chart_checked_date'][] = __('the last chart checked date must less or equal to the current date');
                    $result = false;
                }
            }
        }

        return $result;
    }
}