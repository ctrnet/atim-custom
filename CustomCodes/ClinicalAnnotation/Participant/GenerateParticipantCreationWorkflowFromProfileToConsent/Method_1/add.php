<?php
// --------------------------------------------------------------------------------
// Custom Code Example : Create a participant creation workflow from profile
// to consent
// --------------------------------------------------------------------------------
// MiscIdentifie View Hook
//   - View : add.ctp
//   - Hook() $arg_1 : ''
// Path : \app\Plugin\ClinicalAnnotation\View\MiscIdentifiers\Hook
// File Name : add.php
// --------------------------------------------------------------------------------

// Check if the user is creating a new participant
if (isset($thirdParameterToAddForWorkflow)) {
    // Add new parameter to url to force system to continue the participant creation workflow
    // PARTICIPANT_CREATION_WORKFLOW details:
    //    The third parameters 'PARTICIPANT_CREATION_WORKFLOW' will indicate to the MiscIdentifiers.add() function that the user is
    //    creating a new participant and that next step after the new misc identifier creation will be the creation of a consent.
    //    That will guaranty that user won't be redirected to consent creation form when user will create an additional
    //    misc identifier after the new participant creation workflow completion.
    $finalOptions['links']['top'] .= '/' . $thirdParameterToAddForWorkflow;
}