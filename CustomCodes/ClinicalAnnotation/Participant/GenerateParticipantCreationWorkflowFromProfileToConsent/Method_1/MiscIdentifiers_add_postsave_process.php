<?php
// --------------------------------------------------------------------------------
// Custom Code Example : Create a participant creation workflow from profile 
// to consent
// --------------------------------------------------------------------------------
// MiscIdentifier Controller Hook
// - Controller : MiscIdentifiers
// - Function : add()
// - Hook() $arg_1 : 'postsave_process'
// Path : \app\Plugin\ClinicalAnnotation\Controller\Hook
// File Name : MiscIdentifiers_add_postsave_process.php
// --------------------------------------------------------------------------------

if (isset($this->params['pass']['2']) && $this->params['pass']['2'] == 'PARTICIPANT_CREATION_WORKFLOW') {
    // User is creating a new participant: Try to redirect user to the first active consent creation
    // PARTICIPANT_CREATION_WORKFLOW details:
    //    The third parameters 'PARTICIPANT_CREATION_WORKFLOW' will indicate to the MiscIdentifiers.add() function that the user is
    //    creating a new participant and that next step after the new misc identifier creation will be the creation of a consent.
    //    That will guaranty that user won't be redirected to consent creation form when user will create an additional
    //    misc identifier after the new participant creation workflow completion.
    // Get instance of the ConsentControl model.
    $consentControlModel = AppModel::getInstance('ClinicalAnnotation', 'ConsentControl');
    // Get first active consent control
    $consentControl = $consentControlModel->find('first');
    if ($consentControl) {
        // Redirect to the creation of the participant consent.
        $urlToFlash = '/ClinicalAnnotation/ConsentMasters/add/' . $participantId . '/' . $consentControl['ConsentControl']['id'];
        $this->atimFlash(__('your data has been saved'), $urlToFlash);
    }
}