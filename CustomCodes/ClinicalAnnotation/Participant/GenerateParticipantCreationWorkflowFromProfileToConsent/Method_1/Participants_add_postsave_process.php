<?php
// --------------------------------------------------------------------------------
// Custom Code Example : Create a participant creation workflow from profile
// to consent
// --------------------------------------------------------------------------------
// Participant Controller Hook
// - Controller : Participants
// - Function : add()
// - Hook() $arg_1 : 'postsave_process'
// Path : \app\Plugin\ClinicalAnnotation\Controller\Hook
// File Name : Participants_add_postsave_process.php
// --------------------------------------------------------------------------------

// Try to redirect user to the creation of 'Health Card' identifier then/or first active consent
$miscIdentifierControl = $this->MiscIdentifierControl->find('first', array(
    'conditions' => array(
        'MiscIdentifierControl.misc_identifier_name' => 'ctrnet demo - health card'
    )
));
if ($miscIdentifierControl) {
    // Redirect to the creation of the 'Health Card' misc identifiers.
    // PARTICIPANT_CREATION_WORKFLOW details:
    //    The third parameters 'PARTICIPANT_CREATION_WORKFLOW' will indicate to the MiscIdentifiers.add() function that the user is
    //    creating a new participant and that next step after the new misc identifier creation will be the creation of a consent.
    //    That will guaranty that user won't be redirected to consent creation form when user will create an additional
    //    misc identifier after the new participant creation workflow completion.
    $urlToFlash = '/ClinicalAnnotation/MiscIdentifiers/add/' . $this->Participant->getLastInsertID() . '/' . $miscIdentifierControl['MiscIdentifierControl']['id'] . '/PARTICIPANT_CREATION_WORKFLOW';
} else {
    // No 'Health Card' number can be created. Redirect user to the creation of the first active consent. 
    // Get instance of the ConsentControl model.
    $consentControlModel = AppModel::getInstance('ClinicalAnnotation', 'ConsentControl');
    // Get first active consent control
    $consentControl = $consentControlModel->find('first');
    if ($consentControl) {
        // Redirect to the creation of the participant consent.
        $urlToFlash = '/ClinicalAnnotation/ConsentMasters/add/' . $this->Participant->getLastInsertID() . '/' . $consentControl['ConsentControl']['id'];
    }
}