#################################################################################
#		 Create a participant creation workflow from profile to consent		#
#################################################################################

# CUSTOM CODE DESCRIPTION: 

Speed up the participant creation process creating a Participant Creation Workflow redirecting user to the 'Health Card' 
creation form then to the creation of the first active consent just after the record of a new participant profile.


# SQL STATEMENTS TO RUN:

No sql code needed.


# CUSTOM CODE / HOOK FILES:

METHOD 1

Controller Hook : 
-	app\Plugin\ClinicalAnnotation\Controller\Hook\Participants_add_postsave_process.php
-	app\Plugin\ClinicalAnnotation\Controller\Hook\MiscIdentifiers_add_format.php
-	app\Plugin\ClinicalAnnotation\Controller\Hook\MiscIdentifiers_add_postsave_process.php

View Hook : 
-	\app\Plugin\ClinicalAnnotation\View\MiscIdentifiers\Hook\add.php


# PRINT(S) SCREEN(S): 

Printed screen(s) of before and after update are avalaible to show an overview.


# NOTES:

-