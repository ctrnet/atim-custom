<?php
// --------------------------------------------------------------------------------
// Custom Code Example : Display next Participant Identifier as default value
// --------------------------------------------------------------------------------
// Participant Model Custom
// Path : \app\Plugin\ClinicalAnnotation\Model\Custom
// File Name : Participant.php
// --------------------------------------------------------------------------------

class ParticipantCustom extends Participant
{

    var $useTable = 'participants';

    var $name = "Participant";

    /**
     * Get the next Participant.Id that will be created by the system.
     * 
     * @return int Next participant id 
     */
    public function getNextParticipantIdentifier()
    {
        // Find the last participant id (undeleted) assigned to a participant by the system
        $lastParticipantId = $this->find('first', array(
            'fields' => array(
                'MAX(Participant.id) AS last_participant_id'
            )
        ));
        if (! $lastParticipantId) {
            // No participant exists into the database
            $lastParticipantId = '0';
        } else {
            // Get last participant id
            $lastParticipantId = $lastParticipantId[0]['last_participant_id'];
        }
        // Generate the next participant identifier
        $nextParticipantIdentifier = ($lastParticipantId + 1);
        return $nextParticipantIdentifier;
    }
}