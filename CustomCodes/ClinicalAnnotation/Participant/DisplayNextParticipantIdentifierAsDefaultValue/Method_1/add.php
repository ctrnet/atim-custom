<?php
// --------------------------------------------------------------------------------
// Custom Code Example : Display next Participant Identifier as default value
// --------------------------------------------------------------------------------
// Participant View Hook
//   - View : add.ctp
//   - Hook() $arg_1 : ''
// Path : \app\Plugin\ClinicalAnnotation\View\Participants\Hook
// File Name : add.php
// --------------------------------------------------------------------------------

// Check if the 'nextParticipantIdentifier' has been set in Controller
if (isset($nextParticipantIdentifier)) {
    // Set value to override the default value of the displayed form element 'Participant.participant_identifier'
    $finalOptions['override']['Participant.participant_identifier'] = $nextParticipantIdentifier;
}