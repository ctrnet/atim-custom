<?php
// --------------------------------------------------------------------------------
// Custom Code Example : Display next Participant Identifier as default value
// --------------------------------------------------------------------------------
// Participant Controller Hook
//   - Controller : Participants
//   - Function : add()
//   - Hook() $arg_1 : 'format'
// Path : \app\Plugin\ClinicalAnnotation\Controller\Hook
// File Name : Participants_add_format.php
// --------------------------------------------------------------------------------

// Test if posted data to save exists. If not, that guarantees that we are at the initial display form step.
if(empty($this->request->data)) {
    // Initial display. Set value for the view with the next participant identifier to save to display this value as default value.
    // Call getNextParticipantIdentifier() of the Partcipant model.
    $this->set('nextParticipantIdentifier', $this->Participant->getNextParticipantIdentifier());
}
