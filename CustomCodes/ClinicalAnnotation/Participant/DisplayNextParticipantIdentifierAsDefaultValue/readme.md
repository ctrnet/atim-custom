###############################################################
#	 Display next Participant Identifier as default value	  #
###############################################################

# CUSTOM CODE DESCRIPTION: 

Make ATiM system in charge of the creation of the next default value for field 'Participant.participant_identifier' to display it to the users. 
Value can be saved as is or changed by user in form before to save it.

In the practice, we consider that the next 'Participant.participant_identifier' is equal to the last 'Participant.id' plus 1.


# SQL STATEMENTS TO RUN:

updateParticipantIdentifierInStructureFormats.sql


# CUSTOM CODE / HOOK FILES:

METHOD 1:

Controller Hook : 
-	app\Plugin\ClinicalAnnotation\Controller\Hook\Participants_add_format.php

Model Custom : 
-	\app\Plugin\ClinicalAnnotation\Model\Custom\Participant.php

View Hook : 
-	\app\Plugin\ClinicalAnnotation\View\Participants\Hook\add.php

METHOD 2:

Controller Hook : 
-	app\Plugin\ClinicalAnnotation\Controller\Hook\Participants_add_format.php
-	app\Plugin\ClinicalAnnotation\Controller\Hook\Participants_add_presave_process.php

Model Custom : 
-	\app\Plugin\ClinicalAnnotation\Model\Custom\Participant.php


# PRINT(S) SCREEN(S): 

Printed screen(s) of before and after update are avalaible to show an overview.


# NOTES:

-