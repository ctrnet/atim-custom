<?php
// --------------------------------------------------------------------------------
// Custom Code Example : Display next Participant Identifier as default value
// --------------------------------------------------------------------------------
// Participant Controller Hook
//   - Controller : Participants
//   - Function : add()
//   - Hook() $arg_1 : 'format'
// Path : \app\Plugin\ClinicalAnnotation\Controller\Hook
// File Name : Participants_add_format.php
// --------------------------------------------------------------------------------

// Test if posted data to save exists. If not, that guarantees that we are at the initial display form step.

// Set a variable to indicate to the next part of the function code if we are at the initial display form step or not.
// Variable will be sued by the 'presave_process' hook.
$isInitialDisplayForCustomCode = false;
if (empty($this->request->data)) {
    // Initial display.
    // Set a variable to indicate that we are at the initial display form step.
    $isInitialDisplayForCustomCode = true;
    // Set data of the Participant model with the next participant identifier to save to display this value as default value to the user.
    // Call getNextParticipantIdentifier() of the Partcipant model.
    $this->request->data = array(
        'Participant' => array(
            'participant_identifier' => $this->Participant->getNextParticipantIdentifier()
        )
    );
}