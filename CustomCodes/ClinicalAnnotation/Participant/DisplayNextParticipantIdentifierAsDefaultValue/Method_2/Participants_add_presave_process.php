<?php
// --------------------------------------------------------------------------------
// Custom Code Example : Display next Participant Identifier as default value
// --------------------------------------------------------------------------------
// Participant Controller Hook
//   - Controller : Participants
//   - Function : add()
//   - Hook() $arg_1 : 'presave_process'
// Path : \app\Plugin\ClinicalAnnotation\Controller\Hook
// File Name : Participants_add_presave_process.php
// --------------------------------------------------------------------------------

// Test variable set in 'format' Hook to check if we are at the initial display step or not.
if (isset($isInitialDisplayForCustomCode) && $isInitialDisplayForCustomCode) {
    // Initial display.
    // Force the system to not save the data of the Participant model set in previous Hook to display this data as default value to the user.
    $submittedDataValidates = false;
}