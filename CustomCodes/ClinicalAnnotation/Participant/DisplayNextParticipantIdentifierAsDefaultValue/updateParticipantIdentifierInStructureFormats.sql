UPDATE structure_formats
SET `flag_add`='1', `flag_edit_readonly`='0', `flag_addgrid`='1', `flag_editgrid`='1'
WHERE structure_id=(SELECT id FROM structures WHERE alias='participants')
AND structure_field_id=(
    SELECT id FROM structure_fields WHERE `model`='Participant' AND `tablename`='participants'
    AND `field`='participant_identifier' AND `structure_value_domain` IS NULL AND `flag_confidential`='0'
);