<?php

// --------------------------------------------------------------------------------
// Custom Code Example : Calculate body mass index
// --------------------------------------------------------------------------------
// Participant Model Custom
// Path : \app\Plugin\ClinicalAnnotation\Model\Custom
// File Name : Participant.php
// --------------------------------------------------------------------------------
class ParticipantCustom extends Participant
{

    var $useTable = 'participants';

    var $name = "Participant";

    /**
     * Returns true if all fields pass validation.
     *
     * Will validate the currently set data. Use Model::set() or Model::create() to set the active data.
     *
     * @param array $options
     *            An optional array of custom options to be made available in the beforeValidate callback
     * @return bool True if there are no errors
     */
    public function validates($options = array())
    {
        // Execute parent model validations
        $result = parent::validates($options);

        // Validate that the value 'Height' of the model data to save is not equal to 0 else generate an error (division by 0 error)
        if (array_key_exists('Participant', $this->data) && array_key_exists('ctrnet_workshop_height_m', $this->data['Participant']) && array_key_exists('ctrnet_workshop_weight_kg', $this->data['Participant'])) {
            if (is_numeric($this->data['Participant']['ctrnet_workshop_height_m']) && strlen($this->data['Participant']['ctrnet_workshop_height_m']) && ($this->data['Participant']['ctrnet_workshop_height_m'] * 1) == 0) {
                $this->validationErrors['ctrnet_workshop_height_m'][] = 'the height can not be equal to zero to calculate the bmi';
                $result = false;
            }
        }

        // Return results
        return $result;
    }

    /**
     * Called before each save operation, after validation.
     * Return a non-true result
     * to halt the save.
     *
     * @param array $options
     *            Options passed from Model::save().
     * @return bool True if the operation should continue, false if it should abort
     */
    public function beforeSave($options = array())
    {
        // When both 'Height' and 'Weight' fields are part of the model data to save, calculate the BMI (Body Mass Index) and set a new field to save in database table
        if (array_key_exists('Participant', $this->data) && array_key_exists('ctrnet_workshop_height_m', $this->data['Participant']) && array_key_exists('ctrnet_workshop_weight_kg', $this->data['Participant'])) {
            // Get data to save
            $heightM = $this->data['Participant']['ctrnet_workshop_height_m'];
            $weightKg = $this->data['Participant']['ctrnet_workshop_weight_kg'];
            // Calculate BMI
            $bmi = '';
            if (strlen($heightM) && strlen($weightKg)) {
                if ($heightM == 0) {
                    // No validation to do. Already done by model
                }
                $bmi = $weightKg / ($heightM * $heightM);
            }
            // Add the generated value to the data of the model to save
            $this->data['Participant']['ctrnet_workshop_bmi'] = $bmi;
            // Add value to the AppModel::$writableFields to force system to not remove generated value
            // from the data set to save (feature to prevent hacking comparing fields of the displayed form and submitted data)
            $this->addWritableField('ctrnet_workshop_bmi');
        }

        // Execute parent model beforeSave()
        $retVal = parent::beforeSave($options);

        // Return True if the operation should continue
        return $retVal;
    }
}
