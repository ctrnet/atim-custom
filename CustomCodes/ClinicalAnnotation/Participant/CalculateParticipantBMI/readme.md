################################################
#	 Calculate Participant BMI		 #
################################################

# CUSTOM CODE DESCRIPTION: 

Add 2 fields to participant profile form Height and Weight and add code to calculate participant BMI (kg/m2) automatically based on values entered in previous fields.


# SQL STATEMENTS TO RUN:

createHeightWeightBmiFields.sql


# CUSTOM CODE / HOOK FILES:

METHOD 1:

Model Custom : 
-	\app\Plugin\ClinicalAnnotation\Model\Custom\Participant.php

METHOD 2:

Controller Hook : 
-	app\Plugin\ClinicalAnnotation\Controller\Hook\Participants_add_presave_process.php
-	app\Plugin\ClinicalAnnotation\Controller\Hook\Participants_edit_presave_process.php

Model Custom : 
-	\app\Plugin\ClinicalAnnotation\Model\Custom\Participant.php


# PRINT(S) SCREEN(S): 

Printed screen(s) of before and after update are avalaible to show an overview.


# NOTES:

For any submitted data validation we strongly recommend to use the 'validate()' function of the models.
For any generated data we strongly recommend to use the 'beforeSave()' of 'beforeValidate()' functions of the models (excepted if the generated data will only be generated once).

Also method 2 requires 2 additional hooks. We suggest to limit the number of hook and custom files created to simplify code and future upgrades. 
And the Method 2 won’t guaranty that the value will be updated if another method is created to create/update participant record.
So we consider 1st method as better for long term support and data integrity.
