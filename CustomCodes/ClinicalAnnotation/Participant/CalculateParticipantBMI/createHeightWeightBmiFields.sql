INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`)
VALUES
('ClinicalAnnotation', 'Participant', 'participants', 'ctrnet_workshop_height_m', 'float_positive',  NULL , '0', '', '', '', 'height m', ''),
('ClinicalAnnotation', 'Participant', 'participants', 'ctrnet_workshop_weight_kg', 'float_positive',  NULL , '0', '', '', '', 'weight kg', ''),
('ClinicalAnnotation', 'Participant', 'participants', 'ctrnet_workshop_bmi', 'float_positive',  NULL , '0', '', '', '', 'bmi', '');

INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`)
VALUES
((SELECT id FROM structures WHERE alias='participants'),
(SELECT id FROM structure_fields WHERE `model`='Participant' AND `tablename`='participants' AND `field`='ctrnet_workshop_height_m' AND `type`='float_positive' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='height m' AND `language_tag`=''),
'2', '1', 'body mass Index', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'),
((SELECT id FROM structures WHERE alias='participants'),
(SELECT id FROM structure_fields WHERE `model`='Participant' AND `tablename`='participants' AND `field`='ctrnet_workshop_weight_kg' AND `type`='float_positive' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='weight kg' AND `language_tag`=''),
'2', '2', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'),
((SELECT id FROM structures WHERE alias='participants'),
(SELECT id FROM structure_fields WHERE `model`='Participant' AND `tablename`='participants' AND `field`='ctrnet_workshop_bmi' AND `type`='float_positive' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='bmi' AND `language_tag`=''),
'2', '3', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0');
    
INSERT IGNORE INTO i18n (id,en,fr)
VALUES
('body mass index', 'BMI', 'BMI'),
('height m', 'Height (m)', 'Taille (m)'),
('weight kg', 'Weight (kg)', 'Poids (kg)'),
('the height can not be equal to zero to calculate the bmi', 'The height can not be equal to zero to calculate the bmi.', 'La hauteur ne peut pas être égale à zéro pour calculer le bmi.'),
('bmi', 'BMI', 'BMI');

ALTER TABLE participants 
    ADD COLUMN ctrnet_workshop_height_m decimal (10,2) DEFAULT NULL,
    ADD COLUMN ctrnet_workshop_weight_kg decimal (10,2) DEFAULT NULL,
    ADD COLUMN ctrnet_workshop_bmi decimal (10,2) DEFAULT NULL;

ALTER TABLE participants_revs
    ADD COLUMN ctrnet_workshop_height_m decimal (10,2) DEFAULT NULL,
    ADD COLUMN ctrnet_workshop_weight_kg decimal (10,2) DEFAULT NULL,
    ADD COLUMN ctrnet_workshop_bmi decimal (10,2) DEFAULT NULL;