<?php
// --------------------------------------------------------------------------------
// Custom Code Example : Calculate body mass index
// --------------------------------------------------------------------------------
// Participant Controller Hook
//   - Controller : Participants
//   - Function : add()
//   - Hook() $arg_1 : 'presave_process'
// Path : \app\Plugin\ClinicalAnnotation\Controller\Hook
// File Name : Participants_add_presave_process.php
// --------------------------------------------------------------------------------

// Calculate participant bmi
$this->Participant->calculateParticipantBmi($this->request->data, $submittedDataValidates);
