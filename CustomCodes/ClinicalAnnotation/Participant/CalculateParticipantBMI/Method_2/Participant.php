<?php
// --------------------------------------------------------------------------------
// Custom Code Example : Calculate body mass index
// --------------------------------------------------------------------------------
// Participant Model Custom
// Path : \app\Plugin\ClinicalAnnotation\Model\Custom
// File Name : Participant.php
// --------------------------------------------------------------------------------

class ParticipantCustom extends Participant
{

    var $useTable = 'participants';

    var $name = "Participant";

    /**
     * Calculate the participant BMI based on submitted data
     *
     * @param array $participantData
     *            Submitted data.
     * @param boolean $submittedDataValidates
     *            Boolean defining if the data has been validated or not.
     * @return -
     */
    public function calculateParticipantBmi(&$participantData, &$submittedDataValidates)
    {
        if (array_key_exists('Participant', $participantData) && array_key_exists('ctrnet_workshop_height_m', $participantData['Participant']) && array_key_exists('ctrnet_workshop_weight_kg', $participantData['Participant'])) {
            // Get data to save
            $heightM = str_replace(',', '.', $participantData['Participant']['ctrnet_workshop_height_m']);
            $weightKg = str_replace(',', '.', $participantData['Participant']['ctrnet_workshop_weight_kg']);
            $bmi = null;
            if (strlen($heightM) && strlen($weightKg)) {
                // Validate submitted data
                if (! is_numeric($heightM)) {
                    $this->validationErrors['ctrnet_workshop_height_m'][] = 'error_must_be_float';
                    $submittedDataValidates = false;
                    return;
                }
                if (! is_numeric($weightKg)) {
                    $this->validationErrors['ctrnet_workshop_weight_kg'][] = 'error_must_be_float';
                    $submittedDataValidates = false;
                    return;
                }
                // Validate that the value 'Height' of the model data to save is not equal to 0 else generate an error (division by 0 error)
				if (strlen($heightM) && ($heightM*1) == 0) {
                    $this->validationErrors['ctrnet_workshop_height_m'][] = 'the height can not be equal to zero to calculate the bmi';
                    $submittedDataValidates = false;
                    return;
                }
                // Calculate BMI
                $bmi = $weightKg / ($heightM * $heightM);
            }
            // Add the generated value to the data of the model to save
            $participantData['Participant']['ctrnet_workshop_bmi'] = $bmi;
            // Add value to the AppModel::$writableFields to force system to not remove generated value
            // from the data set to save (feature to prevent hacking comparing fields of the displayed form and submitted data)
            $this->addWritableField('ctrnet_workshop_bmi');
        }
    }
}
    







            
            