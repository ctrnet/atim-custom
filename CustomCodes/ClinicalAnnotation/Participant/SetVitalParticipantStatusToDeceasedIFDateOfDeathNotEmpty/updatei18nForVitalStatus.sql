INSERT IGNORE INTO i18n (id, en, fr) VALUE
('the vital_status is updated to deceased since the participant has a date_of_death', 
'The vital status of the participant is updated to deceased since the participant date of death is defined.',
"Le statut vital du participant est mis à jour en fonction du décès du participant depuis la définition de sa date de décès."); 