###############################################################################################
#	 Set the Vital Status of the participant to deceased if the date of death is not empty	  #
###############################################################################################

# CUSTOM CODE DESCRIPTION: 

Update automatically the participant 'Vital Status' to 'Deceased' when the user set a 'Date of Death' and forget to change the 'Vital Status'.


# SQL STATEMENTS TO RUN:

updatei18nForVitalStatus.sql


# CUSTOM CODE / HOOK FILES:

METHOD 1:

Model Custom : 
-	\app\Plugin\ClinicalAnnotation\Model\Custom\Participant.php

METHOD 2:

Controller Hook : 
-	app\Plugin\ClinicalAnnotation\Controller\Hook\Participants_add_presave_process.php 
-	app\Plugin\ClinicalAnnotation\Controller\Hook\Participants_edit_presave_process.php 


# PRINT(S) SCREEN(S): 

Printed screen(s) of before and after update are avalaible to show an overview.


# NOTES:

Method 2 requires 2 hooks. 
We suggest to limit the number of hook files created to simplify code and future upgrades. 
Also, the Method 2 won’t guaranty that the value will be updated if another method is created to create/update participant record.

So we consider 1st method as better for long term support and data integrity.
