<?php
// --------------------------------------------------------------------------------
// Custom Code Example : Check if date_of_death of the participant is not empty :
//      - Then update field value of vital_status to 'deceased'
//      - And print warning message of the update
// --------------------------------------------------------------------------------
// Participant Controller Hook
//   - Controller : Participants
//   - Function : add()
//   - Hook() $arg_1 : 'presave_process'
// Path : \app\Plugin\ClinicalAnnotation\Controller\Hook
// File Name : Participants_add_presave_process.php
// --------------------------------------------------------------------------------

// If vital_status field is empty or different than 'deceased' and date of death is filled then set vital_status to 'deceased'
if (array_key_exists('Participant', $this->request->data) && array_key_exists('date_of_death', $this->request->data['Participant'])) {
    if (strlen($this->request->data['Participant']['date_of_death']['year']) && array_key_exists('vital_status', $this->request->data['Participant']) && $this->request->data['Participant']['vital_status'] != 'deceased') {
        $this->request->data['Participant']['vital_status'] = 'deceased';
        AppController::addWarningMsg(__('the vital_status is updated to deceased since the participant has a date_of_death'));
    }
}
