<?php
// --------------------------------------------------------------------------------
// Custom Code Example : Check if date_of_death of the participant is not empty :
//      - Then update field value of vital_status to 'deceased'
//      - And print warning message of the update
// --------------------------------------------------------------------------------
// Participant Model Custom
// Path : \app\Plugin\ClinicalAnnotation\Model\Custom
// File Name : Participant.php
// --------------------------------------------------------------------------------

class ParticipantCustom extends Participant
{

    var $useTable = 'participants';

    var $name = "Participant";

    /**
     * Called during validation operations, before validation. 
     *
     * Custom code:
     * Set vital status to deceased when date of death is set.
     *  
     * @param array $options Options passed from Model::save().
     * @return bool True if validate operation should continue, false to abort
     */
    public function beforeValidate($options = array())
    {
        $result = parent::beforeValidate($options);
        
        // If vital_status field is empty or different than 'deceased' and date of death is filled then set vital_status to 'deceased'
        if (array_key_exists('Participant', $this->data) && array_key_exists('date_of_death', $this->data['Participant'])) {
            if (strlen($this->data['Participant']['date_of_death']) && array_key_exists('vital_status', $this->data['Participant']) && $this->data['Participant']['vital_status'] != 'deceased') {
                $this->data['Participant']['vital_status'] = 'deceased';
                AppController::addWarningMsg(__('the vital_status is updated to deceased since the participant has a date_of_death'));
            }
        }
        
        return $result;
    }
}