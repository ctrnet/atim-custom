<?php
// --------------------------------------------------------------------------------
// Custom Code Example : List all consents in participant profile detail form
// --------------------------------------------------------------------------------
// Participant View Hook
//   - View : profile.ctp
//   - Hook() $arg_1 : ''
// Path : \app\Plugin\ClinicalAnnotation\View\Participants\Hook
// File Name : profile.php
// --------------------------------------------------------------------------------

if (! $isAjax) {
    // Display profile form and hide any action button
    $finalOptions['settings']['actions'] = false;
    $this->Structures->build($finalAtimStructure, $finalOptions);
    
    // Prepare properties of the next form to display the list of consents calling the ConsentMasters.lisall() view using the ajaxIndex() function.
    $finalOptions = array(
        'type' => 'detail',
        'links' => array(),
        'settings' => array(
            'header' => __('consents', null),
            'actions' => false
        ),
        'extras' => array(
            'end' => $this->Structures->ajaxIndex('ClinicalAnnotation/ConsentMasters/listall/' . $atimMenuVariables['Participant.id'])
        )
    );
    $finalAtimStructure = array();
}