#######################################################################
#	 		List all consents in participant profile detail form			#
#######################################################################

# CUSTOM CODE DESCRIPTION: 

In 'Participant Profile' form, insert the list of consents of the participant as it is displayed under the 'Clinical Annotation – Consent' menu. 


# SQL STATEMENTS TO RUN:

No sql code to run.


# CUSTOM CODE / HOOK FILES:

METHOD 1

View Hook : 
-	\app\Plugin\ClinicalAnnotation\View\Participants\Hook\profile.php



# PRINT(S) SCREEN(S): 

Printed screen(s) of before and after update are avalaible to show an overview.


# NOTES:

-

