INSERT INTO structures(`alias`) VALUES ('participants_with_no_confidential_info');

SET @structure_id = (SELECT id FROM structures WHERE alias = 'participants_with_no_confidential_info');

INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, 
`flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, 
 `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, 
 `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`,  `flag_float`) 
(SELECT @structure_id, 
structure_formats.structure_field_id, structure_formats.display_column, structure_formats.display_order, structure_formats.language_heading, structure_formats.margin, 
structure_formats.flag_override_label, structure_formats.language_label, structure_formats.flag_override_tag, structure_formats.language_tag, structure_formats.flag_override_help, 
structure_formats.language_help, structure_formats.flag_override_type, structure_formats.type, structure_formats.flag_override_setting, structure_formats.setting, 
structure_formats.flag_override_default, structure_formats.default, structure_formats.flag_add, structure_formats.flag_add_readonly, structure_formats.flag_edit, 
structure_formats.flag_edit_readonly, structure_formats.flag_search, structure_formats.flag_search_readonly, structure_formats.flag_addgrid, structure_formats.flag_addgrid_readonly, 
structure_formats.flag_editgrid, structure_formats.flag_editgrid_readonly, structure_formats.flag_batchedit, structure_formats.flag_batchedit_readonly, structure_formats.flag_index, structure_formats.flag_detail, structure_formats.flag_summary, structure_formats.flag_float
FROM structures 
INNER JOIN structure_formats ON structure_formats.structure_id = structures.id
INNER JOIN structure_fields ON structure_formats.structure_field_id = structure_fields.id
WHERE structures.alias='participants'
AND structure_fields.flag_confidential != '1');