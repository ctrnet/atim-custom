<?php
// ------------------------------------------------------------------------------------
// Custom Code Example : Allow users with no access to confidential info to create 
// participant
// ------------------------------------------------------------------------------------
// Participant Controller Hook
//   - Controller : Participants
//   - Function : add()
//   - Hook() $arg_1 : 'format'
// Path : \app\Plugin\ClinicalAnnotation\Controller\Hook
// File Name : Participants_add_format.php
// --------------------------------------------------------------------------------

// Set a variable with a structure similar than the 'participants' (set in controller)
// but with no confidential fields  to be used in the view to override the default structure.
if (! $this->Session->read('flag_show_confidential')) {
    $this->Structures->set('participants_with_no_confidential_info', 'theParticipantStructureWithNoConfFields');
}