<?php
// --------------------------------------------------------------------------------
// Custom Code Example : Display next Participant Identifier as default value
// --------------------------------------------------------------------------------
// Participant View Hook
// - View : add.ctp
// - Hook() $arg_1 : ''
// Path : \app\Plugin\ClinicalAnnotation\View\Participants\Hook
// File Name : add.php
// --------------------------------------------------------------------------------

if (isset($theParticipantStructureWithNoConfFields)) {
    // Override $finalAtimStructure with a structure with no confidential fields and set in Participant Controller Hook.
    $finalAtimStructure = $theParticipantStructureWithNoConfFields;
}