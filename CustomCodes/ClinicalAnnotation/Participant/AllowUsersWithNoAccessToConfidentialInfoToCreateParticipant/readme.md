#############################################################################
#	Allow users with no access to confidential info to create participant	#
#############################################################################

# CUSTOM CODE DESCRIPTION: 

Allow user being part of a group with no access to confidential information to create a participant using a specific form gathering a list limited to 'no-confidential' fields to complete. 

The code that validates if a user can access a form with confidential information in 'write mode' is executed in the function 'build()' of the StructuresHelper (app\View\Helper\.). 
This code compares the user’s group information and the properties of the fields (flag_confidential) linked to the form used to record data and set in the view (.ctp). 

The solution is to create a 'participants' form with no field flagged as confidential and use this limited structure for any users being part of a group with no access to the confidential information. 


# SQL STATEMENTS TO RUN:

createParticipantWithNoConfidentialInfoStructure.sql


# CUSTOM CODE / HOOK FILES:

METHOD 1:

Controller Hook : 
-	app\Plugin\ClinicalAnnotation\Controller\Hook\Participants_add_format.php

METHOD 2:

Controller Hook : 
-	app\Plugin\ClinicalAnnotation\Controller\Hook\Participants_add_format.php

View Hook : 
-	\app\Plugin\ClinicalAnnotation\View\Participants\Hook\ add.php


# PRINT(S) SCREEN(S): 

-


# NOTES: 

Printed screen(s) of before and after update are avalaible to show an overview.

