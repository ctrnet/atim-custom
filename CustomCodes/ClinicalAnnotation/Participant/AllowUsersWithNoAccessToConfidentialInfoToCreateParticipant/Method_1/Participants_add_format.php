<?php
// ------------------------------------------------------------------------------------
// Custom Code Example : Allow users with no access to confidential info to create 
// participant
// ------------------------------------------------------------------------------------
// Participant Controller Hook
//   - Controller : Participants
//   - Function : add()
//   - Hook() $arg_1 : 'format'
// Path : \app\Plugin\ClinicalAnnotation\Controller\Hook
// File Name : Participants_add_format.php
// --------------------------------------------------------------------------------

// Override the $atimStructure variable (set in controller using the 'participants' structrue alias to be used in the view) 
// by a structure similar but with no confidential fields.
if (! $this->Session->read('flag_show_confidential')) {
	$this->Structures->set('participants_with_no_confidential_info');
}