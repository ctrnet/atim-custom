################################################
#	  Create specific consents 	 #
################################################

# CUSTOM CODE DESCRIPTION: 
Create specific consent a participant has to signed to be part of a study


# SQL STATEMENTS TO RUN:
clinical_annotation_consent_ctrnet_demo_bank.sql


# CUSTOM CODE / HOOK FILES
SQL code do : 
- Update «structure_formats to Hide» 'Consent National' fields
- Create consent_controls for the new consent
- Create db table and/or new consent fields 
- Create structures, structure_fields and structure_formats for consent.


# PRINT(S) SCREEN(S): 
Printed screen(s) of before and after update are avalaible to show an overview.


# COMMENTS:

CTRNet Demo : Demo 'Bank Consent'

- Consent with following fields`
     - agreements
     - biological material use
     - use of urine
     - use of blood
     - use of faeces
     - acces to medical records
     - questionnaire
     - allow questionnaire
     - stop questionnaire
     - stop questionnaire date
     - urine blood use for followup
     - stop followup
     - stop followup date
     - contact agreement
     - contact for additional data
     - inform significant discovery
     - research other disease
     - inform discovery on other disease
