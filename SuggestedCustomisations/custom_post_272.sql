-- ==============================================================================================================================================================
--    CTRNet Custom Script Example
-- ==============================================================================================================================================================
--    CTRNet script proposed as a begining of any custom script developped to install a custom version of ATiM.
-- 
--    @author NL
--    @created 2020-04-03
-- ==============================================================================================================================================================


-- ################################################################################################################################
--   ATiM Core Configuration
-- ################################################################################################################################

-- ATiM name

INSERT IGNORE INTO i18n (id,en,fr) VALUES ('core_installname', 'Name Of The Bank / Install', 'Nom de la banque / installation');

-- Group and users management
--    - Inactivate the force_password_reset option for user id 1
--    - Delete all users except the user with id 1
--    - Create complexe password for all users except the user with id 1
--    - Reanme to 'system' and delete user with id 2 (will be used for any data migration)
--    - Reanme to 'system' and delete group with id 2 

UPDATE groups SET flag_show_confidential = 1 WHERE id = 1;
UPDATE groups SET name = 'System', deleted = 1 WHERE id = 2;

UPDATE users SET flag_active = 1, force_password_reset = 0, password_modified = NOW() WHERE id = 1;
UPDATE users SET flag_active = 0, deleted = 1, password = '87584e77649ff56b76328e08e5da71f604c39451' WHERE id != 1;
UPDATE users SET username = 'system' WHERE id = 2;

SET @system_user_id = (SELECT id FROM users WHERE username LIKE 'system');
SET @created_by = @system_user_id;
SET @user_id = @system_user_id;

-- ################################################################################################################################
--   ATiM Plugins -  Basic Customiszations
--     Clinical Annotation
-- ################################################################################################################################

-- Participant Profile form
-- --------------------------------------------------------------------------------------------------------------------------------

-- Add csv options to search on participant identifiers from csv 

UPDATE structure_formats SET `flag_override_setting`='1', `setting`='size=20,class=file range' WHERE structure_id=(SELECT id FROM structures WHERE alias='participants') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='Participant' AND `tablename`='participants' AND `field`='participant_identifier' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');

-- Add flag_search & flag_index missing

UPDATE structure_formats 
SET `flag_search`='1', `flag_index`='1' 
WHERE structure_id=(SELECT id FROM structures WHERE alias='participants') 
AND flag_add = '1'
AND structure_field_id NOT IN (SELECT id FROM structure_fields WHERE `model`='Participant' AND `field` IN ('ids', 'notes'));

-- Misc Identifiers dates and notes fields
-- --------------------------------------------------------------------------------------------------------------------------------

-- Hide Dates and Notes fields

UPDATE structure_formats SET `flag_add`='0', `flag_edit`='0', `flag_detail`='0' 
WHERE structure_id IN (SELECT id FROM structures WHERE alias IN ('incrementedmiscidentifiers', 'miscidentifiers'))
AND structure_field_id IN (SELECT id FROM structure_fields WHERE `model`='MiscIdentifier' AND `tablename`='misc_identifiers' AND `field` IN ('effective_date', 'expiry_date','notes'));

-- Consent form
-- --------------------------------------------------------------------------------------------------------------------------------

-- Consent status not blank

INSERT INTO `structure_validations` (`id`, `structure_field_id`, `rule`, `on_action`, `language_message`) 
VALUES 
(NULL, 
(SELECT id FROM structure_fields WHERE `model`='ConsentMaster' AND `field`='consent_status'), 'notBlank', '', '');

UPDATE structure_fields SET  `default`='obtained'  WHERE `model`='ConsentMaster' AND `field`='consent_status';

-- Participant Message form
-- --------------------------------------------------------------------------------------------------------------------------------

-- Add flag_search & flag_index missing

UPDATE structure_formats 
SET `flag_search`='1', `flag_index`='1' 
WHERE structure_id=(SELECT id FROM structures WHERE alias='participantmessages') 
AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='ParticipantMessage' AND `tablename`='participant_messages' AND `field`='author' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');

UPDATE structure_formats 
SET `flag_index`='1' 
WHERE structure_id=(SELECT id FROM structures WHERE alias='participantmessages') 
AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='ParticipantMessage' AND `tablename`='participant_messages' AND `field`='description' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');

-- Participant chronology form
-- --------------------------------------------------------------------------------------------------------------------------------

-- Hide Time field

UPDATE structure_formats 
SET `flag_index`='0' 
WHERE structure_id=(SELECT id FROM structures WHERE alias='chronology') 
AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='custom' AND `tablename`='' AND `field`='time' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');


-- ################################################################################################################################
--   ATiM Plugins -  Basic Customiszations
--     Inventory Management
-- ################################################################################################################################

-- Collection view
-- --------------------------------------------------------------------------------------------------------------------------------

-- Add csv options to search on participant identifiers from csv 

UPDATE structure_formats SET `flag_override_setting`='1', `setting`='size=30,class=file range' WHERE structure_id=(SELECT id FROM structures WHERE alias='view_collection') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='ViewCollection' AND `tablename`='' AND `field`='participant_identifier' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');

-- Sample view
-- --------------------------------------------------------------------------------------------------------------------------------

-- Add csv options to search on participant identifiers and sample codes from csv 

UPDATE structure_formats SET `flag_override_setting`='1', `setting`='size=30,class=file range' WHERE structure_id=(SELECT id FROM structures WHERE alias='view_sample_joined_to_collection') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='ViewSample' AND `tablename`='' AND `field`='participant_identifier' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `flag_override_setting`='1', `setting`='size=10,class=file range' WHERE structure_id=(SELECT id FROM structures WHERE alias='view_sample_joined_to_collection') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='ViewSample' AND `field`='sample_code');
UPDATE structure_formats SET `language_heading`='collection' WHERE structure_id=(SELECT id FROM structures WHERE alias='view_sample_joined_to_collection') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='ViewSample' AND `tablename`='' AND `field`='acquisition_label' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');

-- Add headers

UPDATE structure_formats SET `language_heading`='sample' WHERE structure_id=(SELECT id FROM structures WHERE alias='view_sample_joined_to_collection') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='ViewSample' AND `tablename`='' AND `field`='initial_specimen_sample_control_id' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='specimen_sample_type_from_id') AND `flag_confidential`='0');
UPDATE structure_formats SET `language_heading`='sample' WHERE structure_id=(SELECT id FROM structures WHERE alias='view_sample_joined_to_collection') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='ViewSample' AND `tablename`='' AND `field`='initial_specimen_sample_type' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='specimen_sample_type') AND `flag_confidential`='0');

-- Aliquot view
-- --------------------------------------------------------------------------------------------------------------------------------

UPDATE structure_formats SET `flag_override_setting`='1', `setting`='size=30,class=range file' WHERE structure_id=(SELECT id FROM structures WHERE alias='view_aliquot_joined_to_sample_and_collection') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='ViewAliquot' AND `tablename`='' AND `field`='barcode' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `flag_override_setting`='1', `setting`='size=30,class=file range' WHERE structure_id=(SELECT id FROM structures WHERE alias='view_aliquot_joined_to_sample_and_collection') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='ViewAliquot' AND `tablename`='' AND `field`='participant_identifier' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `flag_override_setting`='1', `setting`='size=30,class=range file' WHERE structure_id=(SELECT id FROM structures WHERE alias='view_aliquot_joined_to_sample_and_collection') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='ViewAliquot' AND `tablename`='' AND `field`='aliquot_label' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');

-- Add headers

UPDATE structure_formats SET `language_heading`='collection' WHERE structure_id=(SELECT id FROM structures WHERE alias='view_aliquot_joined_to_sample_and_collection') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='ViewAliquot' AND `tablename`='' AND `field`='acquisition_label' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `language_heading`='sample' WHERE structure_id=(SELECT id FROM structures WHERE alias='view_aliquot_joined_to_sample_and_collection') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='ViewAliquot' AND `tablename`='' AND `field`='initial_specimen_sample_control_id' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='specimen_sample_type_from_id') AND `flag_confidential`='0');
UPDATE structure_formats SET `language_heading`='sample' WHERE structure_id=(SELECT id FROM structures WHERE alias='view_aliquot_joined_to_sample_and_collection') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='ViewAliquot' AND `tablename`='' AND `field`='initial_specimen_sample_type' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='specimen_sample_type') AND `flag_confidential`='0');
UPDATE structure_formats SET `language_heading`='aliquot' WHERE structure_id=(SELECT id FROM structures WHERE alias='view_aliquot_joined_to_sample_and_collection') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='ViewAliquot' AND `tablename`='' AND `field`='aliquot_type' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='aliquot_type') AND `flag_confidential`='0');

-- Collection Template Init form
-- --------------------------------------------------------------------------------------------------------------------------------

-- Add header

UPDATE structure_formats 
SET `language_heading`='specimen' 
WHERE structure_id=(SELECT id FROM structures WHERE alias='template_init_structure') 
AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='SpecimenDetail' AND `tablename`='specimen_details' AND `field`='supplier_dept' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='custom_specimen_supplier_dept') AND `flag_confidential`='0');

-- Add fields

INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='template_init_structure'), 
(SELECT id FROM structure_fields WHERE `model`='DerivativeDetail' AND `tablename`='derivative_details' AND `field`='creation_datetime' AND `type`='datetime' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='inv_creation_datetime_defintion' AND `language_label`='creation date' AND `language_tag`='' LIMIT 0 ,1), 
'2', '100', 'derivative', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'), 
((SELECT id FROM structures WHERE alias='template_init_structure'), 
(SELECT id FROM structure_fields WHERE `model`='DerivativeDetail' AND `tablename`='derivative_details' AND `field`='creation_by' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='custom_laboratory_staff')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='created by' AND `language_tag`=''), 
'2', '200', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'), 
((SELECT id FROM structures WHERE alias='template_init_structure'), 
(SELECT id FROM structure_fields WHERE `model`='DerivativeDetail' AND `tablename`='derivative_details' AND `field`='creation_site' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='custom_laboratory_site')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='creation site' AND `language_tag`=''), 
'2', '300', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'),
((SELECT id FROM structures WHERE alias='template_init_structure'), 
(SELECT id FROM structure_fields WHERE `model`='AliquotMaster' AND `tablename`='aliquot_masters' AND `field`='storage_datetime' AND `type`='datetime' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='inv_initial_storage_datetime_defintion' AND `language_label`='initial storage date' AND `language_tag`=''), 
'3', '1000', 'aliquot', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'),
((SELECT id FROM structures WHERE alias='template_init_structure'), 
(SELECT id FROM structure_fields WHERE `model`='FunctionManagement' AND `tablename`='' AND `field`='recorded_storage_selection_label' AND `type`='autocomplete' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='url=/StorageLayout/StorageMasters/autocompleteLabel' AND `default`='' AND `language_help`='' AND `language_label`='storage' AND `language_tag`='storage selection label'), 
'3', '1001', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'),
((SELECT id FROM structures WHERE alias='template_init_structure'), 
(SELECT id FROM structure_fields WHERE `model`='FunctionManagement' AND `tablename`='' AND `field`='autocomplete_aliquot_master_study_summary_id' AND `type`='autocomplete' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='url=/Study/StudySummaries/autocompleteStudy' AND `default`='' AND `language_help`='' AND `language_label`='study / project' AND `language_tag`=''), 
'3', '1002', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');

-- Collection Form
-- --------------------------------------------------------------------------------------------------------------------------------

-- Make field Bank not blank

UPDATE structure_fields SET `default`='1' WHERE `model`='Collection' AND `tablename`='collections' AND `field`='bank_id';
INSERT INTO `structure_validations` (`id`, `structure_field_id`, `rule`, `on_action`, `language_message`) 
VALUES 
(NULL, 
(SELECT id FROM structure_fields WHERE `model`='Collection' AND `tablename`='collections' AND `field`='bank_id'), 'notBlank', '', '');

-- Tissue sample form
-- --------------------------------------------------------------------------------------------------------------------------------

-- Add flag_search flag_index missing

UPDATE structure_formats 
SET `flag_search`='1', `flag_index`='1' 
WHERE structure_id=(SELECT id FROM structures WHERE alias='sd_spe_tissues') 
AND structure_field_id IN (
  SELECT id 
  FROM structure_fields 
  WHERE `field` IN ('pathology_reception_datetime','tissue_size','tissue_size_unit','tissue_weight','tissue_weight_unit')
);

-- Blood sample form
-- --------------------------------------------------------------------------------------------------------------------------------

-- Add flag_search flag_index missing

UPDATE structure_formats 
SET `flag_search`='1', `flag_index`='1' 
WHERE structure_id=(SELECT id FROM structures WHERE alias='sd_spe_bloods') 
AND structure_field_id IN (
  SELECT id 
  FROM structure_fields 
  WHERE `field` IN ('collected_tube_nbr','collected_volume','collected_volume_unit')
);

-- Change blood types list from fix to custom

INSERT INTO structure_permissible_values_custom_controls (name, flag_active, values_max_length, category) 
VALUES
('Blood Tubes Types', 1, 30, 'inventory');
SET @control_id = (SELECT id FROM structure_permissible_values_custom_controls WHERE name = 'Blood Tubes Types');
SET @user_id = 2;
INSERT INTO structure_permissible_values_customs (`value`, `en`, `fr`, `display_order`, `use_as_input`, `control_id`, `modified`, `created`, `created_by`, `modified_by`) 
VALUES
("unknown", "Unknown", "Inconnu", "100", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("EDTA", "EDTA", "EDTA", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("heparin", "Heparin", "Héparine", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("paxgene", "Paxgene", "Paxgene", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id);
UPDATE structure_permissible_values_custom_controls 
 SET values_used_as_input_counter = 4, values_counter = 4 WHERE name = 'Blood Tubes Types';
UPDATE structure_value_domains SET source = 'StructurePermissibleValuesCustom::getCustomDropdown(\'Blood Tubes Types\')' WHERE domain_name = 'blood_type';
SET @id = (SELECT id FROM structure_value_domains WHERE domain_name = 'blood_type');
UPDATE structure_value_domains_permissible_values SET flag_active = 0 WHERE structure_value_domain_id = @id;

-- Make field blood type not blank

INSERT INTO `structure_validations` (`id`, `structure_field_id`, `rule`, `on_action`, `language_message`) 
VALUES 
(NULL, 
(SELECT id FROM structure_fields WHERE `model`='SampleDetail' AND `tablename`='' AND `field`='blood_type' ), 'notBlank', '', '');

-- Aliquot form
-- --------------------------------------------------------------------------------------------------------------------------------

-- Hide field sop

UPDATE structure_formats SET `flag_add`='0', `flag_edit`='0', `flag_search`='0', `flag_addgrid`='0', `flag_editgrid`='0', `flag_batchedit`='0', `flag_index`='0', `flag_detail`='0' 
WHERE structure_id=(SELECT id FROM structures WHERE alias='aliquot_masters') 
AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='AliquotMaster' AND `tablename`='aliquot_masters' AND `field`='sop_master_id' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='aliquot_sop_list') AND `flag_confidential`='0');

-- Hide field lot number

UPDATE structure_formats SET `flag_add`='0', `flag_edit`='0', `flag_search`='0', `flag_addgrid`='0', `flag_editgrid`='0', `flag_batchedit`='0', `flag_index`='0', `flag_detail`='0' 
WHERE structure_field_id IN (SELECT id FROM structure_fields WHERE `model`='AliquotDetail' AND `tablename`='' AND `field`='lot_number' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');

-- Aliquot block form
-- --------------------------------------------------------------------------------------------------------------------------------

-- Make aliquot block type field not blank

INSERT INTO `structure_validations` (`id`, `structure_field_id`, `rule`, `on_action`, `language_message`) 
VALUES 
(NULL, 
(SELECT id FROM structure_fields WHERE `model`='AliquotDetail' AND `tablename`='' AND `field`='block_type' ), 'notBlank', '', '');

-- Aliquot Tissue tube form
-- --------------------------------------------------------------------------------------------------------------------------------

-- Add storage method and solution

ALTER TABLE ad_tubes
  ADD COLUMN ctrnet_demo_storage_method VARCHAR(100) DEFAULT NULL,
  ADD COLUMN ctrnet_demo_storage_solution VARCHAR(100) DEFAULT NULL;
ALTER TABLE ad_tubes_revs
  ADD COLUMN ctrnet_demo_storage_method VARCHAR(100) DEFAULT NULL,
  ADD COLUMN ctrnet_demo_storage_solution VARCHAR(100) DEFAULT NULL;

UPDATE aliquot_controls 
SET detail_form_alias = CONCAT(detail_form_alias, ',ctrnet_demo_ad_tissue_tubes') 
WHERE sample_control_id = (SELECT id FROM sample_controls WHERE sample_type = 'tissue') 
AND aliquot_type = 'tube';

INSERT INTO structure_value_domains (domain_name, override, category, source) 
VALUES 
("ctrnet_demo_tissue_tube_storage_methods", "", "", CONCAT("StructurePermissibleValuesCustom::getCustomDropdown(\'Tissue Tube Storage Methods\')")),
("ctrnet_demo_tissue_tube_storage_solutions", "", "", CONCAT("StructurePermissibleValuesCustom::getCustomDropdown(\'Tissue Tube Storage Solutions\')"));
INSERT INTO structure_permissible_values_custom_controls (name, flag_active, values_max_length, category) 
VALUES 
("Tissue Tube Storage Methods", 1, 50, 'inventory'),
("Tissue Tube Storage Solutions", 1, 50, 'inventory');
SET @control_id = (SELECT id FROM structure_permissible_values_custom_controls WHERE name = "Tissue Tube Storage Methods");
INSERT INTO `structure_permissible_values_customs` (`value`, en, fr, `use_as_input`, `control_id`, `modified_by`, `created`, `created_by`, `modified`)
VALUES
('flash freeze','Flash Freeze', "", '1', @control_id, @user_id, NOW(),@user_id, NOW()),
('none','None', "Aucune", '1', @control_id, @user_id, NOW(),@user_id, NOW());
SET @control_id = (SELECT id FROM structure_permissible_values_custom_controls WHERE name = "Tissue Tube Storage Solutions");
INSERT INTO `structure_permissible_values_customs` (`value`, en, fr, `use_as_input`, `control_id`, `modified_by`, `created`, `created_by`, `modified`)
VALUES
('DMSO','', "", '1', @control_id, @user_id, NOW(),@user_id, NOW()),
('OCT','', "", '1', @control_id, @user_id, NOW(),@user_id, NOW()),
('none','None', "Aucune", '1', @control_id, @user_id, NOW(),@user_id, NOW());

INSERT INTO structures(`alias`) VALUES ('ctrnet_demo_ad_tissue_tubes');
INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) VALUES
('InventoryManagement', 'AliquotDetail', 'ad_tubes', 'ctrnet_demo_storage_method', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='ctrnet_demo_tissue_tube_storage_methods') , '0', '', '', '', 'storage method', ''), 
('InventoryManagement', 'AliquotDetail', 'ad_tubes', 'ctrnet_demo_storage_solution', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='ctrnet_demo_tissue_tube_storage_solutions') , '0', '', '', '', 'storage solution', '');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) VALUES 
((SELECT id FROM structures WHERE alias='ctrnet_demo_ad_tissue_tubes'), (SELECT id FROM structure_fields WHERE `model`='AliquotDetail' AND `tablename`='ad_tubes' AND `field`='ctrnet_demo_storage_method' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='ctrnet_demo_tissue_tube_storage_methods')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='storage method' AND `language_tag`=''), '1', '74', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='ctrnet_demo_ad_tissue_tubes'), (SELECT id FROM structure_fields WHERE `model`='AliquotDetail' AND `tablename`='ad_tubes' AND `field`='ctrnet_demo_storage_solution' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='ctrnet_demo_tissue_tube_storage_solutions')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='storage solution' AND `language_tag`=''), '1', '74', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '1', '1', '0', '0');

INSERT IGNORE INTO i18n (id,en,fr) 
VALUES
('storage method', 'Storage Method', "Méthode d'entreposage"),
('storage solution', 'Storage Solution', "Solution d'entreposage");

-- Other Aliquot tube forms
-- --------------------------------------------------------------------------------------------------------------------------------

-- Add storage method and solution

-- ALTER TABLE ad_tubes
--   ADD COLUMN ctrnet_demo_storage_method VARCHAR(100) DEFAULT NULL,
--   ADD COLUMN ctrnet_demo_storage_solution VARCHAR(100) DEFAULT NULL;
-- ALTER TABLE ad_tubes_revs
--   ADD COLUMN ctrnet_demo_storage_method VARCHAR(100) DEFAULT NULL,
--   ADD COLUMN ctrnet_demo_storage_solution VARCHAR(100) DEFAULT NULL;

UPDATE aliquot_controls 
SET detail_form_alias = CONCAT(detail_form_alias, ',ctrnet_demo_ad_other_tubes_storage_solutions_and_methods') 
WHERE sample_control_id != (SELECT id FROM sample_controls WHERE sample_type = 'tissue') 
AND detail_tablename = 'ad_tubes'
AND detail_form_alias != '' AND detail_form_alias IS NOT NULL;
UPDATE aliquot_controls 
SET detail_form_alias = 'ctrnet_demo_ad_other_tubes_storage_solutions_and_methods'
WHERE sample_control_id != (SELECT id FROM sample_controls WHERE sample_type = 'tissue') 
AND detail_tablename = 'ad_tubes'
AND (detail_form_alias = '' OR detail_form_alias IS NULL);

INSERT INTO structure_value_domains (domain_name, override, category, source) 
VALUES 
("ctrnet_demo_other_tube_storage_methods", "", "", CONCAT("StructurePermissibleValuesCustom::getCustomDropdown(\'Other Tube Storage Methods\')")),
("ctrnet_demo_other_tube_storage_solutions", "", "", CONCAT("StructurePermissibleValuesCustom::getCustomDropdown(\'Other Tube Storage Solutions\')"));
INSERT INTO structure_permissible_values_custom_controls (name, flag_active, values_max_length, category) 
VALUES 
("Other Tube Storage Methods", 1, 50, 'inventory'),
("Other Tube Storage Solutions", 1, 50, 'inventory');
SET @control_id = (SELECT id FROM structure_permissible_values_custom_controls WHERE name = "Other Tube Storage Methods");
INSERT INTO `structure_permissible_values_customs` (`value`, en, fr, `use_as_input`, `control_id`, `modified_by`, `created`, `created_by`, `modified`)
VALUES
('none','None', "Aucune", '1', @control_id, @user_id, NOW(),@user_id, NOW());
SET @control_id = (SELECT id FROM structure_permissible_values_custom_controls WHERE name = "Other Tube Storage Solutions");
INSERT INTO `structure_permissible_values_customs` (`value`, en, fr, `use_as_input`, `control_id`, `modified_by`, `created`, `created_by`, `modified`)
VALUES
('none','None', "Aucune", '1', @control_id, @user_id, NOW(),@user_id, NOW());

INSERT INTO structures(`alias`) VALUES ('ctrnet_demo_ad_other_tubes_storage_solutions_and_methods');
INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) VALUES
('InventoryManagement', 'AliquotDetail', 'ad_tubes', 'ctrnet_demo_storage_method', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='ctrnet_demo_other_tube_storage_methods') , '0', '', '', '', 'storage method', ''), 
('InventoryManagement', 'AliquotDetail', 'ad_tubes', 'ctrnet_demo_storage_solution', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='ctrnet_demo_other_tube_storage_solutions') , '0', '', '', '', 'storage solution', '');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) VALUES 
((SELECT id FROM structures WHERE alias='ctrnet_demo_ad_other_tubes_storage_solutions_and_methods'), (SELECT id FROM structure_fields WHERE `model`='AliquotDetail' AND `tablename`='ad_tubes' AND `field`='ctrnet_demo_storage_method' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='ctrnet_demo_other_tube_storage_methods')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='storage method' AND `language_tag`=''), '1', '74', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='ctrnet_demo_ad_other_tubes_storage_solutions_and_methods'), (SELECT id FROM structure_fields WHERE `model`='AliquotDetail' AND `tablename`='ad_tubes' AND `field`='ctrnet_demo_storage_solution' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='ctrnet_demo_other_tube_storage_solutions')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='storage solution' AND `language_tag`=''), '1', '74', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '1', '1', '0', '0');

INSERT IGNORE INTO i18n (id,en,fr) 
VALUES
('storage method', 'Storage Method', "Méthode d'entreposage"),
('storage solution', 'Storage Solution', "Solution d'entreposage");

-- Aliquot Internal Use
-- --------------------------------------------------------------------------------------------------------------------------------

-- Re-order field type and use code field of the aliquot internal use form

UPDATE structure_formats SET `display_order`='2' WHERE structure_id=(SELECT id FROM structures WHERE alias='aliquotinternaluses') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='AliquotInternalUse' AND `tablename`='aliquot_internal_uses' AND `field`='use_code' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `display_order`='1' WHERE structure_id=(SELECT id FROM structures WHERE alias='aliquotinternaluses') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='AliquotInternalUse' AND `tablename`='aliquot_internal_uses' AND `field`='type' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='aliquot_internal_use_type') AND `flag_confidential`='0');

UPDATE structure_fields SET  `setting`='size=15',  `default`='n/a' WHERE model='AliquotInternalUse' AND tablename='aliquot_internal_uses' AND field='use_code' AND `type`='input' AND structure_value_domain  IS NULL ;

-- ################################################################################################################################
--   ATiM Plugins -  Basic Customiszations
--     Tool
-- ################################################################################################################################

-- Study Tool : New design (based on 
-- --------------------------------------------------------------------------------------------------------------------------------

-- Add institution to study summary form.

ALTER TABLE study_summaries 
  ADD COLUMN ctrnet_demo_institution VARCHAR(50) DEFAULT NULL;
ALTER TABLE study_summaries_revs 
  ADD COLUMN ctrnet_demo_institution VARCHAR(50) DEFAULT NULL;

INSERT INTO structure_value_domains (domain_name, source) 
VALUES 
('ctrnet_demo_institutions', "StructurePermissibleValuesCustom::getCustomDropdown('Institutions')");
INSERT INTO structure_permissible_values_custom_controls (name, flag_active, values_max_length, category) 
VALUES 
('Institutions', 1, 50, 'study / project');

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('Study', 'StudySummary', 'study_summaries', 'ctrnet_demo_institution', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='ctrnet_demo_institutions') , '0', '', '', '', 'laboratory / institution', '');

INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES 
((SELECT id FROM structures WHERE alias='studysummaries'), 
(SELECT id FROM structure_fields WHERE `model`='StudySummary' AND `tablename`='study_summaries' AND `field`='ctrnet_demo_institution'), 
'1', '8', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '0');

INSERT IGNORE INTO i18n (id,en,fr) 
VALUES
('laboratory / institution', 'Laboratory/Institution','Laboratoire/Institution');

-- Add ethical information to study summary form.

ALTER TABLE study_summaries 
  ADD COLUMN ctrnet_demo_ethical_approved char(1) DEFAULT '', 
  ADD COLUMN ctrnet_demo_ethical_approval_file_name varchar(500) DEFAULT null;
ALTER TABLE study_summaries_revs 
  ADD COLUMN ctrnet_demo_ethical_approved char(1) DEFAULT '', 
  ADD COLUMN ctrnet_demo_ethical_approval_file_name varchar(500) DEFAULT null;
  
INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('Study', 'StudySummary', 'study_summaries', 'ctrnet_demo_ethical_approved', 'yes_no',  NULL , '0', '', '', '', 'ethic', ''), 
('Study', 'StudySummary', 'study_summaries', 'ctrnet_demo_ethical_approval_file_name', 'input',  NULL , '0', 'size=50', '', '', '', 'file name');

INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES 
((SELECT id FROM structures WHERE alias='studysummaries'), 
(SELECT id FROM structure_fields WHERE `model`='StudySummary' AND `tablename`='study_summaries' AND `field`='ctrnet_demo_ethical_approved' AND `type`='yes_no' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='ethic' AND `language_tag`=''), 
'2', '1', 'approval', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='studysummaries'), 
(SELECT id FROM structure_fields WHERE `model`='StudySummary' AND `tablename`='study_summaries' AND `field`='ctrnet_demo_ethical_approval_file_name' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=50' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`='file name'), 
'2', '2', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0');

INSERT IGNORE INTO i18n (id,en,fr) 
VALUES
('approval', 'Approval', 'Approbation'),
('ethic', 'Ethic', 'éthique'),
('file name', 'File Name', 'Nom du fichier');

-- Add MTA information to study summary form.

ALTER TABLE study_summaries 
  ADD COLUMN ctrnet_demo_mta_data_sharing_approved char(1) DEFAULT '', 
  ADD COLUMN ctrnet_demo_mta_data_sharing_approved_file_name varchar(500) DEFAULT null;
ALTER TABLE study_summaries_revs 
  ADD COLUMN ctrnet_demo_mta_data_sharing_approved char(1) DEFAULT '', 
  ADD COLUMN ctrnet_demo_mta_data_sharing_approved_file_name varchar(500) DEFAULT null; 
   
INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('Study', 'StudySummary', 'study_summaries', 'ctrnet_demo_mta_data_sharing_approved', 'yes_no',  NULL , '0', '', '', '', 'mta data sharing', ''), 
('Study', 'StudySummary', 'study_summaries', 'ctrnet_demo_mta_data_sharing_approved_file_name', 'input',  NULL , '0', 'size=50', '', '', '', 'file name');

INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES 
((SELECT id FROM structures WHERE alias='studysummaries'), 
(SELECT id FROM structure_fields WHERE `model`='StudySummary' AND `tablename`='study_summaries' AND `field`='ctrnet_demo_mta_data_sharing_approved' AND `type`='yes_no' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='mta data sharing' AND `language_tag`=''), 
'2', '3', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='studysummaries'), 
(SELECT id FROM structure_fields WHERE `model`='StudySummary' AND `tablename`='study_summaries' AND `field`='ctrnet_demo_mta_data_sharing_approved_file_name' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=50' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`='file name'), 
'2', '4', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0');

INSERT IGNORE INTO i18n (id,en,fr) 
VALUES
('file name', 'File Name', 'Nom du fichier'),
('mta data sharing', 'MTA Data Sharing', 'Partage de matériels et de données');

-- Add PubmedIds to track Pubmed references.

ALTER TABLE study_summaries 
  ADD COLUMN ctrnet_demo_pubmed_ids TEXT DEFAULT NULL;
ALTER TABLE study_summaries_revs 
  ADD COLUMN ctrnet_demo_pubmed_ids TEXT DEFAULT NULL;  

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('Study', 'StudySummary', 'study_summaries', 'ctrnet_demo_pubmed_ids', 'textarea',  NULL , '0', 'cols=40,rows=1', '', '', 'pubmed ids', '');

INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES 
((SELECT id FROM structures WHERE alias='studysummaries'), 
(SELECT id FROM structure_fields WHERE `model`='StudySummary' AND `tablename`='study_summaries' AND `field`='ctrnet_demo_pubmed_ids' AND `type`='textarea' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='cols=40,rows=1' AND `default`='' AND `language_help`='' AND `language_label`='pubmed ids' AND `language_tag`=''), 
'2', '20', 'literature', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0');

INSERT IGNORE INTO i18n (id,en,fr) 
VALUES
('literature','Literature','Littérature'),
('pubmed ids','PubMed IDs','PubMed IDs');

-- Change position of the 'Summary' field.

UPDATE structure_formats 
SET `display_order`='30', `language_heading`='details' 
WHERE structure_id=(SELECT id FROM structures WHERE alias='studysummaries') 
AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='StudySummary' AND `tablename`='study_summaries' AND `field`='summary' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');

-- Hide field 'Summary' Disease Site'.

UPDATE structure_formats 
SET `flag_add`='0', `flag_edit`='0', `flag_search`='0', `flag_index`='0', `flag_detail`='0', `flag_summary`='0' 
WHERE structure_id=(SELECT id FROM structures WHERE alias='studysummaries') 
AND structure_field_id IN (
  SELECT id 
  FROM structure_fields 
  WHERE `model`='StudySummary' AND `tablename`='study_summaries' AND `field`='disease_site'
);  

-- Redesign the Study Investigator sub-form.

UPDATE structure_formats SET `flag_add`='0', `flag_edit`='0', `flag_index`='0', `flag_detail`='0', `flag_summary`='0' WHERE structure_id=(SELECT id FROM structures WHERE alias='studyinvestigators') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='StudyInvestigator' AND `tablename`='study_investigators' AND `field`='middle_name' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `flag_add`='0', `flag_edit`='0', `flag_detail`='0' WHERE structure_id=(SELECT id FROM structures WHERE alias='studyinvestigators') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='StudyInvestigator' AND `tablename`='study_investigators' AND `field`='occupation' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `flag_add`='0', `flag_edit`='0', `flag_detail`='0' WHERE structure_id=(SELECT id FROM structures WHERE alias='studyinvestigators') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='StudyInvestigator' AND `tablename`='study_investigators' AND `field`='department' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `flag_index`='0', `flag_summary`='0' WHERE structure_id=(SELECT id FROM structures WHERE alias='studyinvestigators') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='StudyInvestigator' AND `tablename`='study_investigators' AND `field`='organization' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `flag_add`='0', `flag_edit`='0', `flag_detail`='0' WHERE structure_id=(SELECT id FROM structures WHERE alias='studyinvestigators') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='StudyInvestigator' AND `tablename`='study_investigators' AND `field`='participation_start_date' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `flag_add`='0', `flag_edit`='0', `flag_detail`='0' WHERE structure_id=(SELECT id FROM structures WHERE alias='studyinvestigators') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='StudyInvestigator' AND `tablename`='study_investigators' AND `field`='participation_end_date' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `display_column`='1', `display_order`='-1' WHERE structure_id=(SELECT id FROM structures WHERE alias='studyinvestigators') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='StudyInvestigator' AND `tablename`='study_investigators' AND `field`='role' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='role') AND `flag_confidential`='0');
UPDATE structure_fields SET  `language_label`='study_city',  `language_tag`='' WHERE model='StudyInvestigator' AND tablename='study_investigators' AND field='address_city' AND `type`='input' AND structure_value_domain  IS NULL ;
UPDATE structure_fields SET  `language_label`='study_province',  `language_tag`='' WHERE model='StudyInvestigator' AND tablename='study_investigators' AND field='address_province' AND `type`='input' AND structure_value_domain  IS NULL ;
UPDATE structure_fields SET  `language_label`='study_country',  `language_tag`='' WHERE model='StudyInvestigator' AND tablename='study_investigators' AND field='address_country' AND `type`='input' AND structure_value_domain  IS NULL ;
UPDATE structure_fields SET  `language_label`='study_street',  `language_tag`='' WHERE model='StudyInvestigator' AND tablename='study_investigators' AND field='address_street' AND `type`='input' AND structure_value_domain  IS NULL ;
UPDATE structure_formats SET `display_column`='1', `display_order`='50' WHERE structure_id=(SELECT id FROM structures WHERE alias='studyinvestigators') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='StudyInvestigator' AND `tablename`='study_investigators' AND `field`='brief' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `language_heading`='study_address' WHERE structure_id=(SELECT id FROM structures WHERE alias='studyinvestigators') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='StudyInvestigator' AND `tablename`='study_investigators' AND `field`='address_street' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `flag_index`='1' WHERE structure_id=(SELECT id FROM structures WHERE alias='studyinvestigators') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='StudyInvestigator' AND `tablename`='study_investigators' AND `field`='organization' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `flag_index`='1' WHERE structure_id=(SELECT id FROM structures WHERE alias='studyinvestigators') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='StudyInvestigator' AND `tablename`='study_investigators' AND `field`='email' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `flag_index`='1' WHERE structure_id=(SELECT id FROM structures WHERE alias='studyinvestigators') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='StudyInvestigator' AND `tablename`='study_investigators' AND `field`='role' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='role') AND `flag_confidential`='0');

-- Redesign the Study Funding sub-form.

UPDATE structure_formats SET `flag_add`='0', `flag_edit`='0', `flag_addgrid`='0', `flag_editgrid`='0', `flag_index`='0', `flag_detail`='0', `flag_summary`='0' WHERE structure_id=(SELECT id FROM structures WHERE alias='studyfundings') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='StudyFunding' AND `tablename`='study_fundings' AND `field`='restrictions' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `flag_add`='0', `flag_edit`='0', `flag_detail`='0' WHERE structure_id=(SELECT id FROM structures WHERE alias='studyfundings') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='StudyFunding' AND `tablename`='study_fundings' AND `field`='amount_year_1' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `flag_add`='0', `flag_edit`='0', `flag_detail`='0' WHERE structure_id=(SELECT id FROM structures WHERE alias='studyfundings') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='StudyFunding' AND `tablename`='study_fundings' AND `field`='year_1' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `flag_add`='0', `flag_edit`='0', `flag_detail`='0' WHERE structure_id=(SELECT id FROM structures WHERE alias='studyfundings') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='StudyFunding' AND `tablename`='study_fundings' AND `field`='amount_year_2' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `flag_add`='0', `flag_edit`='0', `flag_detail`='0' WHERE structure_id=(SELECT id FROM structures WHERE alias='studyfundings') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='StudyFunding' AND `tablename`='study_fundings' AND `field`='year_2' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `flag_add`='0', `flag_edit`='0', `flag_detail`='0' WHERE structure_id=(SELECT id FROM structures WHERE alias='studyfundings') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='StudyFunding' AND `tablename`='study_fundings' AND `field`='amount_year_3' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `flag_add`='0', `flag_edit`='0', `flag_detail`='0' WHERE structure_id=(SELECT id FROM structures WHERE alias='studyfundings') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='StudyFunding' AND `tablename`='study_fundings' AND `field`='year_3' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `flag_add`='0', `flag_edit`='0', `flag_detail`='0' WHERE structure_id=(SELECT id FROM structures WHERE alias='studyfundings') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='StudyFunding' AND `tablename`='study_fundings' AND `field`='amount_year_4' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `flag_add`='0', `flag_edit`='0', `flag_detail`='0' WHERE structure_id=(SELECT id FROM structures WHERE alias='studyfundings') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='StudyFunding' AND `tablename`='study_fundings' AND `field`='year_4' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `flag_add`='0', `flag_edit`='0', `flag_detail`='0' WHERE structure_id=(SELECT id FROM structures WHERE alias='studyfundings') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='StudyFunding' AND `tablename`='study_fundings' AND `field`='amount_year_5' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `flag_add`='0', `flag_edit`='0', `flag_detail`='0' WHERE structure_id=(SELECT id FROM structures WHERE alias='studyfundings') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='StudyFunding' AND `tablename`='study_fundings' AND `field`='year_5' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');

-- Bank
-- --------------------------------------------------------------------------------------------------------------------------------

-- Change bank name

UPDATE banks SET name = 'Complete bank name';
UPDATE banks_revs SET name = 'Complete bank name';

-- SOP
-- --------------------------------------------------------------------------------------------------------------------------------

-- Change Sop controls names

UPDATE sop_controls SET sop_group = 'inventory', detail_form_alias = 'sopd_inventory_all';
UPDATE sop_controls SET type = 'collection' WHERE id = 1;
UPDATE sop_controls SET type = 'other' WHERE id = 2;

-- Change Sop controls names

UPDATE structure_fields SET  `default`='activated' WHERE model='SopMaster' AND tablename='sop_masters' AND field='status' AND `type`='select' AND structure_value_domain =(SELECT id FROM structure_value_domains WHERE domain_name='sop_status');
INSERT INTO `structure_validations` (`id`, `structure_field_id`, `rule`, `on_action`, `language_message`) 
VALUES 
(NULL, 
(SELECT id FROM structure_fields WHERE model='SopMaster' AND tablename='sop_masters' AND field='status'), 'notBlank', '', '');

-- ################################################################################################################################
--   ATiM Plugins -  Optional Customiszations
-- ################################################################################################################################

UPDATE structure_formats 
SET `flag_add`='0', `flag_edit`='0', `flag_addgrid`='0', `flag_editgrid`='0', `flag_index`='0', `flag_detail`='0', `flag_summary`='0' 
WHERE structure_field_id IN (SELECT id FROM structure_fields WHERE `field`='acquisition_label');

DELETE FROM structure_validations WHERE structure_field_id IN (SELECT id FROM structure_fields WHERE field = 'acquisition_label');
